import { useEffect } from 'react';

const useScript = (url, type = 'external', callback) => {
  useEffect(() => {
    const script = document.createElement('script');
    if (type === 'inline') {
      const inlineScript = document.createTextNode(url);
      script.appendChild(inlineScript);
    }
    else {
      script.src = url;
      script.async = true;
    }

    document.body.appendChild(script);
    script.addEventListener("load", function() {
      if (callback) callback(); 
    });

    return () => {
      document.body.removeChild(script);
    }
  }, [url]);
};

export default useScript;