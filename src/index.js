import React from "react";
import ReactDOM from "react-dom";
import * as firebase from "firebase";
import App from "./app/App";
// import { ReactComponent as ProperlyLoader } from "./assets/img/loader.png";

const firebaseConfig = {
  apiKey: "AIzaSyDP5bKRgh3SW8yQBOk6V0LrcTUAl46n0cI",
  authDomain: "loopin-fad04.firebaseapp.com",
  databaseURL: "https://loopin-fad04.firebaseio.com",
  projectId: "loopin-fad04",
  storageBucket: "loopin-fad04.appspot.com",
  messagingSenderId: "323879034068",
  appId: "1:323879034068:web:3bd872628ae3aadc23ef41",
  measurementId: "G-52KWY2EQGW"
};
let app;

export const initialize = () => {
  app = firebase.initializeApp(firebaseConfig);
}
export const isInitialized = () => !!app;
ReactDOM.render(<App />, document.getElementById("root"));





