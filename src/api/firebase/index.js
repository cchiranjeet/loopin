import axios from 'axios';

function loadAxiosInstance() {
  const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_FIREBASE_API_ENDPOINT + '/',
    headers: {
      "Content-Type": "application/json"
    },
    transformResponse: (response) => {
      return JSON.parse(response);
    }
  });

  return axiosInstance;
}

export function getApi() {
  return loadAxiosInstance();
}