import { getApi } from './index';

export function sendEmail(template, payload) {
  return new Promise((resolve, reject) => {
    getApi().post(`${template}`, JSON.stringify(payload))
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject((error.response && error.response.data.error) || "error");
      });
  });
}

export function getBot(plan) {
  return new Promise((resolve, reject) => {
    getApi().get(`bot?plan=${plan}`)
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject((error.response && error.response.data.error) || "error");
      });
  });
}

export function getBotSubscription(email, plan) {
  return new Promise((resolve, reject) => {
    getApi().get(`bot-subscription?email=${email}&plan=${plan}`)
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject((error.response && error.response.data.error) || "error");
      });
  });
}

export function purchaseBotSubscription(payload) {
  return new Promise((resolve, reject) => {
    getApi().post(`purchase-bot-subscription`, JSON.stringify(payload))
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject((error.response && error.response.data.error) || "error");
      });
  });
}