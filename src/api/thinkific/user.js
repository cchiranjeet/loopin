import { getApi } from './index';

export function createThinkificUser(payload) {
  return new Promise((resolve, reject) => {
    getApi().post(`users`, JSON.stringify(payload))
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject((error.response && error.response.data.error) || "error");
      });
  });
}

export function getThinkificUserByEmail(email) {
  return new Promise((resolve, reject) => {
    getApi().get(`users?query[email]=${email}`)
      .then(response => {
        resolve(response.data.items[0] || false);
      })
      .catch(error => {
        reject((error.response && error.response.data.error) || "error");
      });
  });
}