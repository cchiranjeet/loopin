import { getApi } from './index';

export function getThinkificProduct(productId) {
  return new Promise((resolve, reject) => {
    getApi().get(`products/${productId}`)
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject((error.response && error.response.data.error) || "error");
      });
  });
}