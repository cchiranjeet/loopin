import axios from 'axios';

function loadAxiosInstance() {
  const axiosInstance = axios.create({
    baseURL: 'https://api.thinkific.com/api/public/v1/',
    headers: {
      "Content-Type": "application/json",
      "X-Auth-API-Key": process.env.REACT_APP_THINKIFIC_API_KEY,
      "X-Auth-Subdomain": process.env.REACT_APP_THINKIFIC_API_SUB_DOMAIN
    },
    transformResponse: (response) => {
      return JSON.parse(response);
    }
  });

  return axiosInstance;
}

export function getApi() {
  return loadAxiosInstance();
}