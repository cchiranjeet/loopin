import { getApi } from './index';

export function createThinkificEnrollment(payload) {
  return new Promise((resolve, reject) => {
    getApi().post(`enrollments`, JSON.stringify(payload))
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject((error.response && error.response.data.error) || "error");
      });
  });
}

export function getThinkificEnrollment(userId, courseId) {
  return new Promise((resolve, reject) => {
    getApi().get(`enrollments?query[user_id]=${userId}&query[course_id]=${courseId}`)
      .then(response => {
        resolve(response.data.items[0] || false);
      })
      .catch(error => {
        reject((error.response && error.response.data.error) || "error");
      });
  });
}