function importAll(r) {
  const keys = r.keys();
  const images = {};
  for (var i = 0; i < keys.length; i++) {
    const imageName = keys[i].replace('./', '');
    images[imageName] = require(`${keys[i]}`);
  }
  return images;
}

const blogImages = importAll(require.context('./', false, /\.(png|jpe?g|svg)$/));

export default blogImages;