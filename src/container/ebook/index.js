/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';
import Popup from "../../component/modal";
import Loader from '../../component/loader';
import KamalAanjna from "../../assets/img/Kamal-Aanjna.jpg";

import { usePurchase } from "../../component/purchase";
import "firebase/auth";
import "firebase/firestore";
import { initialize, isInitialized } from "../../index";

const coursePrice = 547,
  thinkificCourseId = 669453,
  thinkificProductId = 704449,
  courseName = 'Network Marketing 5.0 - Handbook';

class Ebook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        username: props.loggedIn?.displayName || '',
        emailid: props.loggedIn?.email || '',
        mobileno: props.loggedIn?.phoneNumber || ''
      },
      showPopup: false,
      errors: {},
      loading: false
    };
    this.handleChange = this.handleChange.bind(this);
    props.setShowSection(true);
    this.purchase = usePurchase({ setLoading: this.setLoading });
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    if (!isInitialized()) {
      initialize();
    }
  }

  setLoading = (loading) => {
    this.setState({ loading: loading });
  }

  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm = (e) => {
    e.preventDefault();
    this.validateForm(async () => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        // const { emailid, message, username } = this.state.fields;
        // fetch(
        //   `https://us-central1-loopin-fad04.cloudfunctions.net/sendMail?to=${emailid}&subject=Payment&message=Payment Gateway`,
        //   {
        //     method: "POST"
        //   }
        // );

        this.setState({ loading: true });
        try {
          const purchaseResponse = await this.purchase.purchaseCourse({
            email: this.state.fields.emailid,
            username: this.state.fields.username,
            user: this.props.loggedIn,
            courseName,
            thinkificCourseId,
            thinkificProductId
          });
          // SEND CONFIMATION EMAIL
          await fetch(`${process.env.REACT_APP_FIREBASE_API_ENDPOINT}/ebookPayment`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify(purchaseResponse.confirmEmailParams)
          });
          this.props.history.push(this.props.location.pathname + '/thank-you');
        } catch (err) { } finally {
          this.setState({ loading: false });
        }

      }
    });
  }

  OpenPopup = () => {
    this.setState({
      showPopup: true
    });
    document.body.style.position = "fixed";
    document.body.style.overflow = "Hidden";
  };
  closePopup = () => {
    this.setState({
      showPopup: false
    });
    document.body.style.position = "unset";
    document.body.style.overflow = "unset";
  };

  validateUserName = cb => {
    const {
      fields: { username }
    } = this.state;

    if (username.length !== 0) {
      if (!username.match(/^[a-zA-Z ]*$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: "*Please enter alphabet characters only."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            username: "*Please enter your full name"
          }
        },
        cb
      );
    }
  };

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };
  validateMobileNo = cb => {
    const {
      fields: { mobileno }
    } = this.state;

    if (mobileno.length !== 0) {
      if (!mobileno.match(/^[0-9]{10}$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              mobileno: "*Please enter valid mobile no."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              mobileno: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            mobileno: "*Please enter your mobile no."
          }
        },
        cb
      );
    }
  };
  validateForm = cb => {
    this.validateMobileNo(() =>
      this.validateEmailID(() => this.validateUserName(cb))
    );
  };
  render() {
    return (
      <>
        <Loader show={this.state.loading} />
        <div className="lopin-container">
          <div className="banner ebook">
            {/* <img src={banner1} alt="banner" /> */}
            <div className="banner-content">
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Col sm="12">
                    <div className="heading" style={{ fontSize: "22px" }}>
                      A HANDBOOK FOR THE NEW-AGE NETWORK MARKETERS <br />
                    Experience the new avatar
                    <br /> of Network Marketing
                  </div>
                    <a>
                      <div className="download" onClick={this.OpenPopup}>
                        Watch Promo
                    </div>
                    </a>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
          <div className="content-details">
            <Container fluid style={{ maxWidth: "1200px" }} id="view">
              <Row>
                <Col sm={12}>
                  <div className="heading ebook">
                    INSIDE OF THIS HANDBOOK, HERE ARE A FEW <br />
                  THINGS THAT YOU’LL BE GETTING
                </div>
                  <div className="subtitle">
                    This book is the secret to building an ultra-successful
                    network marketing business. So, if you a new-age network
                    marketer, then this is the book for you. Read to discover the
                    new avatar of Network Marketing.
                </div>
                </Col>
              </Row>
              <Row>
                <Col sm={7}>
                  <div className="content-wrapper">
                    <p style={{ fontSize: "20px", marginBottom: "10px" }}>
                      In this book you’ll learn
                  </p>
                    <ul className="point">
                      <li>Past, Present and Future of Network Marketing.</li>
                      <li>
                        Why you should immediately quit your job and become a
                        network marketer?
                    </li>
                      <li>Understanding of supplement income and its need.</li>
                      <li>Chances of failure and the absence of guarantee.</li>
                      <li>Why you should say goodbye to old-school ways.</li>
                      <li>How be prepare yourself for the next revolution.</li>
                      <li>
                        The easiest way to reach out to new people and grow your
                        business.
                    </li>
                    </ul>
                    <p style={{ fontSize: "18px", marginTop: "20px" }}>
                      This book is a short, compact and to the point. Read fast
                      and change your life faster.
                  </p>
                  </div>
                </Col>
                <Col sm={5}>
                  <form
                    className="form_wrapeer purchase-form"
                    method="post"
                    name="userRegistrationForm"
                    onSubmit={this.submituserRegistrationForm}
                  >
                    <div className="offer">
                      Special Offer Price <strike>₹2,000</strike>
                      <span>₹{coursePrice}</span>
                    </div>
                    <div className="heading">
                      GET YOUR HANDBOOK <br />
                    WITHIN A FEW MINUTES{" "}
                    </div>
                    <div className="input-wrap">
                      <input
                        type="text"
                        name="username"
                        placeholder="Name"
                        autoComplete="false"
                        value={this.state.fields.username}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.username}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <input
                        type="text"
                        autoComplete="false"
                        placeholder="Email"
                        name="emailid"
                        value={this.state.fields.emailid}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.emailid}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <input
                        type="text"
                        name="mobileno"
                        placeholder="Contact number"
                        value={this.state.fields.mobileno}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.mobileno}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <button type="submit" className="button">
                        Yes! I want this book
                      </button>
                    </div>
                  </form>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="sample-book">
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col sm={8} xs={12}>
                  <div className="pickbook">
                    <div className="heading">Peek-a-book</div>
                    <div className="subheading">
                      Read the first few pages of the book now.
                  </div>
                  </div>
                </Col>
                <Col sm={4} xs={12}>
                  <div className="preview">
                    <div className="button">
                      <Link to="/pdfview">Preview Book</Link>
                    </div>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="about-author">
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col sm="12">
                  <div className="heading">About the Author</div>
                </Col>
              </Row>
              <Row>
                <Col sm={6} offset={{ sm: 3 }} xs={12}>
                  <div className="visionary-section">
                    <Row>
                      <Col sm={12}>
                        <div className="wrap">
                          <div className="visionary-imgb pull-left sajeev" style={{ backgroundImage: `URL(${KamalAanjna})`, backgroundPositionX: 'right' }}></div>
                          <div className="visionary-txtb">
                            Kamal Aanjna
                          <span>Idea Generator, Data Geek</span>
                            <a
                              target="_blank"
                              href=""
                              className="icon-linked-in clearfix"
                            ></a>
                          </div>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm={12}>
                        <p>
                          Kamal Aanjna is an Indian entrepreneur, an avid investor
                          and consultant in various sectors including digital and
                          real estate. His current venture deals with start-ups
                          and focuses on the investment aspect of the business.
                          Furthermore, after gaining experience from working with
                          renowned brands like HCL technologies, American Express,
                          and Nimbus, he co-founded &amp; partnered companies like
                          Alliance Teletech, Dharohar Realty, and Sunjoss Internet
                          among his other successful ventures. When not busting
                          major marketing &amp; investment moves, Kamal spends his
                          time with his wife, son, and daughter. He is a fitness
                          freak and loves butter coffee and all things healthy.
                      </p>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
          {this.state.showPopup && (
            <Popup
              text='Click "Close Button" to hide popup'
              closePopup={this.closePopup}
              videoType={'youtube'}
              videoId={'eHVP5QDZsGM'}
            // url="https://firebasestorage.googleapis.com/v0/b/loopin-fad04.appspot.com/o/E%20Book%20Promo%20-%20ENGLISH.mp4?alt=media&token=128452e5-0420-43ff-ba81-03d22500ac7b"
            />
          )}
        </div>
      </>
    );
  }
}

export default Ebook;
