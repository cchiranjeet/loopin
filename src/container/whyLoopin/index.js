/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import {Link} from "react-router-dom";
import { Container, Row, Col } from "react-grid-system";
// import Popup from "../../assets/img/bookpopup.jpg";
import Ebookart from "../../assets/img/ebookart.jpg";
import Popup from "../../component/modal";

class WhyLoopin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false
    };

    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  OpenPopup = () => {
    this.setState({
      showPopup: true
    });
    document.body.style.position = "fixed";
    document.body.style.overflow = "Hidden";
  };
  closePopup = () => {
    this.setState({
      showPopup: false
    });
    document.body.style.position = "unset";
    document.body.style.overflow = "unset";
  };

  render() {
    return (
      <div className="lopin-container">
        <div className="banner banner2">
          {/* <img src={banner1} /> */}
          <div className="banner-content">
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col sm="12">
                  <div className="heading" style={{ fontSize: "22px" }}>
                    Experience the new avatar
                    <br /> of Network Marketing
                  </div>
                  <a>
                    <div className="download" onClick={this.OpenPopup}>
                      Watch Promo
                    </div>
                  </a>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
        <div className="content-details">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={12}>
                <div className="heading">Why LOOPIN?</div>
              </Col>
            </Row>
            <Row>
              <Col sm={8}>
                <div className="content-wrapper">
                  <div className="content">
                    To make your network marketing business successful and earn
                    more money, you need more people to talk to about your
                    business. You need to be efficient with your time; spending
                    less time talking to people who are not interested, and more
                    time talking to people who exactly know they want what you
                    have to offer.
                    <br />
                    <br />
                    We believe the next stage of your business growth requires
                    new technology and strategy because the conventional
                    methodology and techniques that are being used today won’t
                    get you there. Hence, LOOPIN.
                    <br />
                    <br />
                    Knowing & understanding the growing needs of the network
                    marketing industry. Therefore, we’ve built a tool that
                    tracks, manages, sends messages, creates lists, schedules
                    and shares everything using just one single dashboard -
                    freeing up more time for you to focus more on expanding your
                    network.
                    <br />
                    <br />
                    No tech to learn, nothing to download, LOOPIN offers fun and
                    engaging ‘Create Your Adventure’ for prospects. Supporting
                    both customer acquisition and biz opportunity, LOOPIN helps
                    minimize procrastination and rejection. So, now, avoid the
                    pain of rejection and get going.
                    <br />
                    <br />
                    Built specially for the 21st-century network marketer,
                    LOOPIN works on your phone, tablet or computer. All data
                    synced between the web and the app, so you can travel
                    worry-free wherever your business takes you.
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <Link to="/whatloopin" className="popup-image">
                  <img src={Ebookart} />
                </Link>
              </Col>
            </Row>
          </Container>
        </div>
        {this.state.showPopup ? (
          <Popup
            text='Click "Close Button" to hide popup'
            closePopup={this.closePopup}
            videoType={'youtube'}
            videoId={'IVODnJRKCoo'}
          />
        ) : null}
      </div>
    );
  }
}

export default WhyLoopin;
