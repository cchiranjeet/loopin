/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";

class Faq extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="faq">
        <Container fluid style={{ maxWidth: "1200px" }}>
          <Row>
            <Col sm={12}>
              <div className="heading">Privacy Policy</div>
              {/* <div className="subheading"></div> */}
              <div className="wrapper">
                <div className="answer">
                  Your privacy is important to us. It is Loopin IT Innovations, LLP&#39;s policy to respect your
                  privacy regarding any information we may collect from you across our website,
                  loopinglobal.com, and other sites we own and operate.
                  <br />
                  <br />
                  We only ask for personal information when we truly need it to provide a service to you. We
                  collect it by fair and lawful means, with your knowledge and consent. We also let you know
                  why we’re collecting it and how it will be used.
                  <br />
                  <br />
                  We only retain collected information for as long as necessary to provide you with your
                  requested service. What data we store, we’ll protect within commercially acceptable means
                  to prevent loss and theft, as well as unauthorized access, disclosure, copying, use or
                  modification.
                  <br />
                  <br />
                  We don’t share any personally identifying information publicly or with third-parties, except
                  when required to by law.
                  <br />
                  <br />
                  Our website may link to external sites that are not operated by us. Please be aware that we
                  have no control over the content and practices of these sites, and cannot accept
                  responsibility or liability for their respective privacy policies.
                  <br />
                  <br />
                  You are free to refuse our request for your personal information, with the understanding that
                  we may be unable to provide you with some of your desired services.
                  <br />
                  <br />
                  Your continued use of our website will be regarded as acceptance of our practices around
                  privacy and personal information. If you have any questions about how we handle user data
                  and personal information, feel free to contact us.
                  <br />
                  <br />
                  This policy is effective as of 1 April 2020.
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Faq;
