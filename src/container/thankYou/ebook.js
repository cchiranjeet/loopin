import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";
import ThankYouBanner from "../../assets/img/on-the-way.jpg";

class ThankYou extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="lopin-container">
        <div className="banner" style={{ backgroundImage: `url(${ThankYouBanner})` }}></div>
        <div className="content-details">
          <Container fluid style={{ maxWidth: "1200px" }} className="">
            <Row>
              <Col xs={12}>
                <div className="content-wrapper">
                  <div className="content text-center">
                    <h2>Congratulations! You’ve successfully signed up for Kamal Aanjna’s Success Blueprint Course</h2>
                    <br />
                    <h3>Thank you. We promise to support you throughout your journey.</h3>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  }
}

export default ThankYou;

