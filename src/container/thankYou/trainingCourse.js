/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";
import ThankYouBanner from "../../assets/img/IT-ALL-STARTS-TODAY.jpg";

class ThankYou extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="lopin-container">
      <div className="banner" style={{ backgroundImage: `url(${ThankYouBanner})` }}></div>
        <div className="content-details">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col xs={12}>
                <div className="content-wrapper">
                  <div className="content text-center">
                    <h1>THE ACCESS TO YOUR HANDBOOK IS ON THE WAY!</h1>
                    <br />
                    <h3>You shall be receiving a confirmation shortly.</h3>
                    <h3>If you face any problem, please feel free to contact us.</h3>
                    <br />
                    <strong>Now that you’ve purchased the book, make sure you put it to good use.</strong>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  }
}

export default ThankYou;
