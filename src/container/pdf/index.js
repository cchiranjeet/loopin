/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { PDFReader, MobilePDFReader } from "reactjs-pdf-reader";
import Pdf from "../../assets/pdf/PDF_500kB.pdf";
import { Visible, Hidden } from "react-grid-system";
// import { Link } from "react-router-dom";

class PdfViwer extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: false };

    props.setShowSection(false);
  }

  componentDidMount() {
    document.addEventListener("contextmenu", this._handleContextMenu);
    document.addEventListener("click", this._handleClick);
    document.addEventListener("scroll", this._handleScroll);
  }

  componentWillUnmount() {
    document.removeEventListener("contextmenu", this._handleContextMenu);
    document.removeEventListener("click", this._handleClick);
    document.removeEventListener("scroll", this._handleScroll);
  }

  _handleContextMenu = event => {
    event.preventDefault();

    this.setState({ visible: true });

    const clickX = event.clientX;
    const clickY = event.clientY;
    const screenW = window.innerWidth;
    const screenH = window.innerHeight;
    const rootW = this.root.offsetWidth;
    const rootH = this.root.offsetHeight;

    const right = screenW - clickX > rootW;
    const left = !right;
    const top = screenH - clickY > rootH;
    const bottom = !top;

    if (right) {
      this.root.style.left = `${clickX + 5}px`;
    }

    if (left) {
      this.root.style.left = `${clickX - rootW - 5}px`;
    }

    if (top) {
      this.root.style.top = `${clickY + 5}px`;
    }

    if (bottom) {
      this.root.style.top = `${clickY - rootH - 5}px`;
    }
  };

  _handleClick = event => {
    const { visible } = this.state;
    const wasOutside = !(event.target.contains === this.root);

    if (wasOutside && visible) this.setState({ visible: false });
  };

  //   _handleScroll = () => {
  //     const { visible } = this.state;

  //     if (visible) this.setState({ visible: false });
  //   };

  render() {
    // const { visible } = this.state;

    return (
      <div className="pdf-container">
        <div className="goBack" onClick={this.props.history.goBack}>
          <i class="left"></i> Go back
        </div>
        <div
          className="pdf"
          ref={ref => {
            this.root = ref;
          }}
        >
          <Hidden xs sm>
            <PDFReader showAllPage="true" url={Pdf} />
          </Hidden>
          <Visible xs sm>
            <div style={{ overflow: "scroll", height: "100vh" }}>
              <MobilePDFReader
                isShowHeader={false}
                isShowFooter={false}
                scale="auto"
                url={Pdf}
              />
            </div>
          </Visible>
        </div>
      </div>
    );
  }
}

export default PdfViwer;
