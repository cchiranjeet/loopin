/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";
import Popup from "../../assets/img/what.gif";
import BannerImage from "../../assets/img/whatloopin.jpg";

class WhatLoopin extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="lopin-container">
        <div className="banner" style={{backgroundImage: `url(${BannerImage})`}}>{/* <img src={banner1} /> */}</div>
        <div className="content-details">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={12}>
                <div className="heading">What is LOOPIN?</div>
              </Col>
            </Row>
            <Row>
              <Col sm={8}>
                <div className="content-wrapper">
                  <div className="content">
                    How can I find more people who want to talk to me about my
                    business? This is one question that has been haunting every
                    network marketer until today. Agree or not, once you have
                    gone beyond your initial list, adding new names or engaging
                    new prospects can be a challenge. <br />
                    <br />
                    LOOPIN is the end of all this horror. Yes, you read that
                    right, LOOPIN lets you create an unwavering moment where you
                    can generate consistent and sustainable leads who are super
                    interested in your opportunity and products. An integrated
                    architecture with built-in intelligence and communication
                    between two Facebook Chabot delivers innovative sales
                    enabled solutions that will help you build a good downline,
                    creating multiplier-effect in your growth and income.
                    <br />
                    <br />
                    <div className="subheading">
                      WAIT, THERE’S NO <br />
                      CATCH HERE!
                    </div>
                    With an objective to solving all your network marketing
                    woes, LOOPIN is a simplified, streamlined, and focused
                    network marketing tool that is changing the way network
                    marketers are managing contacts, connecting with prospects,
                    sharing content, tracking, reporting, following up and
                    building their business.
                    <br />
                    <br />
                    If you are looking to add more direct sales enthusiasts to
                    your team beyond just your network of friends and family,
                    then Loopin is the answer.{" "}
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="popup-image">
                  <img src={Popup} />
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  }
}

export default WhatLoopin;
