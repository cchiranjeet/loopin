/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col, Hidden, Visible } from "react-grid-system";
import { Link } from "react-router-dom";
// import { Tabs, Tab, TabPanel, TabList } from "react-web-tabs";
import "react-web-tabs/dist/react-web-tabs.css";
import CartImage from "../../assets/img/cart.png";

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = { CartVaule: 1 };

    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="cart-container">
        {this.props.loggedIn && this.state.CartVaule > 0 ? (
          <div className="cart-details">
            <Hidden xs sm>
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Col sm={12}>
                    <div className="breadcrumbs">
                      <ul>
                        <li>Cart</li>
                        <li className="arrow"></li>
                        <li className="active">Payment</li>
                      </ul>
                    </div>
                    <div className="payment">
                      <div className="first-section">
                        <div className="heading">Order summary</div>
                        <table cellPadding="0" cellSpacing="0">
                          <tr>
                            <th></th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Sum</th>
                            <th />
                          </tr>
                          {this.props.cartValue.map(x => (
                            <tr>
                              <td>
                                <div className="product-image" />
                              </td>
                              <td>{x.name}</td>
                              <td>1</td>
                              <td>
                                <div className="unit-price">₹{x.price}</div>
                                {/* <strike>₹2000</strike> */}
                              </td>
                              <td>₹{x.price}</td>
                              <td
                                style={{ cursor: "pointer", color: "#0086f5" }}
                                onClick={() => {
                                  this.props.setCartValue(prevState => ([
                                    ...prevState.filter(y => y.name !== x.name)
                                  ]));
                                }}
                              >
                                remove
                              </td>
                            </tr>
                          ))}
                        </table>
                        <div className="choose-payment">
                          <div className="heading">Choose Payment Method</div>
                          {/* <Tabs defaultTab="vertical-tab-one" vertical>
                            <TabList>
                              <Tab tabFor="vertical-tab-one">
                                Debit/Credit Card
                              </Tab>
                              <Tab tabFor="vertical-tab-two">Net Banking</Tab>
                              <Tab tabFor="vertical-tab-three">
                                PHONEPE/GOOGLE PAY/BHIM UPI
                              </Tab>
                              <Tab tabFor="vertical-tab-four">PAYTM/WALLET</Tab>
                            </TabList>
                            <TabPanel tabId="vertical-tab-one">
                              <p>Credit card payment</p>
                            </TabPanel>
                            <TabPanel tabId="vertical-tab-two">
                              <p>Net Banking payment</p>
                            </TabPanel>
                            <TabPanel tabId="vertical-tab-three">
                              <p>UPI Payment</p>
                            </TabPanel>
                            <TabPanel tabId="vertical-tab-four">
                              <p>wallet</p>
                            </TabPanel>
                          </Tabs> */}
                          <div style={{ marginTop: 20 }}>Coming Soon...</div>
                        </div>
                      </div>
                      <div className="last-section">
                        <div className="heading">Order summary</div>
                        <table cellPadding="0" cellSpacing="0">
                          <tr>
                            <td>Order Total</td>
                            <td>₹200</td>
                          </tr>
                          <tr>
                            <td>Tax</td>
                            <td>₹20</td>
                          </tr>
                          <tr>
                            <td className="total">Order Total</td>
                            <td className="total">₹200</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Container>
            </Hidden>
            <Visible xs sm>
              <Container fluid>
                <Row>
                  <Col xs={12}>
                    {/* <div className="breadcrumbs">
                    <ul>
                      <li>Cart</li>
                      <li className="arrow"></li>
                      <li className="active">Payment</li>
                    </ul>
                  </div> */}
                    <div className="heading">Order details</div>
                    <table cellSpacing="0" cellPadding="0">
                      <tr>
                        <td>Product Name</td>
                        <td>E-book XYZ E-book</td>
                      </tr>
                      <tr>
                        <td>Quantity</td>
                        <td>1</td>
                      </tr>
                      <tr>
                        <td>Unit Price</td>
                        <td>
                          <strike>`2000</strike>
                          <span> 500</span>
                        </td>
                      </tr>
                      <tr>
                        <td>Total Price</td>
                        <td>500</td>
                      </tr>
                    </table>
                    <div className="heading">Price Details</div>
                    <table
                      className="price-details"
                      cellSpacing="0"
                      cellPadding="0"
                    >
                      <tr>
                        <td>Order Total</td>
                        <td className="price"> Rs 200</td>
                      </tr>
                      <tr>
                        <td>Tax</td>
                        <td className="price">Rs 20</td>
                      </tr>
                      <tr>
                        <td className="total">Total</td>
                        <td className="price total">220</td>
                      </tr>
                    </table>
                    <button type="button" className="pay">
                      Pay
                    </button>
                  </Col>
                </Row>
              </Container>
            </Visible>
          </div>
        ) : (
          <div>
            {this.state.CartVaule === 0 && this.props.loggedIn ? (
              <div className="emptycart">
                <div className="cart-image">
                  <img src={CartImage} />
                </div>
                <div className="cart-text">Your cart is empty!</div>
                <div className="subheading">
                  It's a good day to buy the items you saved for later!
                </div>
                {/* <div className="button">
                <Link to="/signin">Login</Link>
              </div> */}
              </div>
            ) : (
              <div className="emptycart">
                <div className="cart-image">
                  <img src={CartImage} />
                </div>
                <div className="cart-text">Missing Cart items?</div>
                <div className="subheading">
                  Login to see the items you added previously
                </div>
                <div className="button">
                  <Link to="/signin">Login</Link>
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default Cart;
