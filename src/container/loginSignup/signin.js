/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import "swiper/css/swiper.css";
import * as firebase from "firebase";
import "firebase/auth";
import { Container, Row, Col } from "react-grid-system";
import { Link } from "react-router-dom";
import Google from "../../assets/img/google.png";
// import scrollToComponent from "react-scroll-to-component";
// import { ReactComponent as Share } from "../../assets/img/Share.svg";
import { initialize, isInitialized } from "../../index";

class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
      fields: {
        emailid: "",
        password: ""
      },
      errors: {},
      loading: false,
      googleLoading: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
    props.setShowSection(false);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    if (!isInitialized()) {
      initialize();
    }
  }
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  signUpWithGoogle = () => {
    this.setState({ googleLoading: true });
    const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
    firebase
      .auth()
      .signInWithPopup(googleAuthProvider)
      .then(result => {
        const token = result.credential.accessToken;
        const user = result.user;
        this.setState({ googleLoading: false });
        this.props.setLoggedIn(user);
        localStorage.setItem("token", token);
        this.props.history.push("/");
      })
      .catch(error => {
        console.log(error);
        this.setState({ googleLoading: false });
      });
  };

  submituserRegistrationForm(e) {
    e.preventDefault();
    this.validateForm(() => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        // this.props.updateSuccessState(true);
        this.setState({ loading: true });

        firebase
          .auth()
          .signInWithEmailAndPassword(
            this.state.fields.emailid,
            this.state.fields.password
          )
          .catch(error => {
            console.log(error);
            this.setState({ loading: false, message: error.message });
          });

        firebase.auth().onAuthStateChanged(user => {
          console.log(user);
          if (user) {
            this.setState({ loading: false });
            this.props.setLoggedIn(user);
            this.props.history.push("/");
          } else {
            this.props.setLoggedIn(null);
          }
        });
      }
    });
  }

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };

  validatePassword = cb => {
    const {
      fields: { password }
    } = this.state;
    if (password.length === 0) {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            password: "*Please enter your Password"
          }
        },
        cb
      );
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            password: undefined
          }
        },
        cb
      );
    }
  };

  forgetPassword = () => {
    this.validateEmailID(() => {
      if (!this.state.errors.emailid) {
        firebase
          .auth()
          .sendPasswordResetEmail(this.state.fields.emailid)
          .then(() => {
            this.setState({ message: "Email has been sent to your email" });
          })
          .catch(error => {
            this.setState({
              errors: { ...this.state.errors, emailid: error.message }
            });
          });
      }
    });
  };

  validateForm = cb => {
    this.validatePassword(() => this.validateEmailID(cb));
  };
  render() {
    return (
      <div className="bg-white">
        <Container fluid>
          <div className="login-container">
            <Row>
              <Col sm={8} xs={8}>
                <div className="login-title">
                  Don't have account? <Link to="/signup">Register</Link>
                </div>
              </Col>
              <Col sm={4} xs={4}>
                <div className="skip" onClick={this.props.history.goBack}>
                  Skip
                </div>
              </Col>
            </Row>
            <Row justify="center">
              <Col sm={6}>
                <div className="login-wrapper">
                  <div className="header">Continue LOOPIN</div>
                  <div className="subheader">
                    Log in to your account so you continue LOOPIN <br /> your
                    prospects & grow your network.
                  </div>
                </div>
                <form
                  className="form_wrapeer "
                  method="post"
                  name="userRegistrationForm"
                  onSubmit={this.submituserRegistrationForm}
                >
                  <div className="input-wrap">
                    <label className="label">
                      Email address<span>*</span>
                    </label>
                    <input
                      type="text"
                      autoComplete="false"
                      name="emailid"
                      value={this.state.fields.emailid}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.emailid}
                    </span>
                  </div>
                  <div className="input-wrap">
                    <label className="label">
                      Password<span>*</span>
                    </label>
                    <input
                      type="password"
                      name="password"
                      autoComplete="false"
                      value={this.state.fields.password}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.password}
                    </span>
                  </div>
                  <div onClick={this.forgetPassword}>
                    <div className="forgot-password">Forgot Password</div>
                  </div>
                  <div className="error-message">{this.state.message}</div>
                  <div className="input-wrap">
                    <button type="submit" className="button">
                      {this.state.loading ? "Signing in..." : "Sign in"}
                    </button>
                  </div>
                </form>
                {/* <div className="divider">
                  <span className="line" />
                  <span className="or">OR</span>
                  <span className="line" />
                </div>
                <div className="g-btn" onClick={() => this.signUpWithGoogle()}>
                  <img src={Google} alt="" />
                  {this.state.googleLoading
                    ? "Signing up with Google..."
                    : "Signing with Google"}
                </div> */}
              </Col>
            </Row>
            <Row justify="center">
              <Col sm={12}>
                <div className="login-footer">
                  <div className="heading">
                    Grow your network marketing business today!
                  </div>
                  <div className="subheading">
                    We’re committed to your privacy. Loopin uses the information
                    you provide to us to contact you about our relevant content,
                    products, and services. You may unsubscribe from these
                    communications at any time. For more information, check out
                    our Privacy policy.
                  </div>
                  <div className="line-divider" />
                  <div className="copy">
                    ©2019 Loopin, Inc. All Rights Reserved.
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
    );
  }
}

export default Signin;
