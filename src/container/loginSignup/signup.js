/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import * as firebase from "firebase";
import "firebase/auth";
import "swiper/css/swiper.css";
import { Container, Row, Col } from "react-grid-system";
import { Link } from "react-router-dom";
import Google from "../../assets/img/google.png";
// import scrollToComponent from "react-scroll-to-component";
// import { ReactComponent as Share } from "../../assets/img/Share.svg";
import { initialize, isInitialized } from "../../index";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
      fields: {
        username: "",
        emailid: "",
        password: ""
      },
      errors: {},
      loading: false,
      googleLoading: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
    props.setShowSection(false);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    if (!isInitialized()) {
      initialize();
    }
  }
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  signUpWithGoogle = () => {
    this.setState({ googleLoading: true });
    const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
    googleAuthProvider.setCustomParameters({
      prompt: "select_account"
    });
    firebase
      .auth()
      .signInWithPopup(googleAuthProvider)
      .then(result => {
        const token = result.credential.accessToken;
        const user = result.user;
        this.setState({ googleLoading: false });
        this.props.setLoggedIn(user);
        localStorage.setItem("token", token);
        this.props.history.push("/");
      })
      .catch(error => {
        console.log(error);
        this.setState({ googleLoading: false });
      });
  };

  createThinkificUser = () => {
    fetch("https://api.thinkific.com/api/public/v1/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-Auth-API-Key": "d08d117c3917d14cf9f1e8fa188c2ec5",
        "X-Auth-Subdomain": "loopin"
      },
      body: JSON.stringify({
        first_name: this.state.fields.username.split(" ")[0],
        last_name: this.state.fields.username.split(" ")[1],
        email: this.state.fields.emailid,
        password: this.state.fields.password
      })
    })
      .then(x => x.json())
      .then(
        response => console.log(response),
        response => console.log(response)
      );
  };

  submituserRegistrationForm = e => {
    e.preventDefault();
    this.validateForm(() => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        this.setState({ loading: true });

        firebase
          .auth()
          .createUserWithEmailAndPassword(
            this.state.fields.emailid,
            this.state.fields.password
          )
          .catch(error => {
            console.log(error);
            this.setState({ loading: false, message: error.message });
          });

        firebase.auth().onAuthStateChanged(user => {
          if (user) {
            this.setState({ loading: false });
            this.props.setLoggedIn(user);

            this.createThinkificUser();
            user
              .updateProfile({ displayName: this.state.fields.username })
              .then(() => {
                const nextUser = firebase.auth().currentUser;
                this.props.setLoggedIn(user);
              });

            this.props.history.push("/");
          } else {
            this.props.setLoggedIn(null);
          }
        });
      }
    });
  };

  validateUserName = cb => {
    const {
      fields: { username }
    } = this.state;

    if (username.length !== 0) {
      if (!username.match(/^[a-zA-Z ]*$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: "*Please enter alphabet characters only."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            username: "*Please enter your full name"
          }
        },
        cb
      );
    }
  };

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };

  validatePassword = cb => {
    const {
      fields: { password }
    } = this.state;
    if (password.length === 0) {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            password: "*Please enter your Password"
          }
        },
        cb
      );
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            password: undefined
          }
        },
        cb
      );
    }
  };

  validateForm = cb => {
    this.validatePassword(() =>
      this.validateEmailID(() => this.validateUserName(cb))
    );
  };
  render() {
    return (
      <div className="bg-white">
        <Container fluid>
          <div className="login-container">
            <Row>
              <Col sm={8} xs={8}>
                <div className="login-title">
                  Have an account?<Link to="/signin"> Sign in.</Link>
                </div>
              </Col>
              <Col sm={4} xs={4}>
                <div className="skip" onClick={this.props.history.goBack}>
                  Skip
                </div>
              </Col>
            </Row>
            <Row justify="center">
              <Col sm={6}>
                <div className="login-wrapper">
                  <div className="header">Looking to LOOPIN?</div>
                  <div className="subheader">
                    Create a free account today <br />& find people who are
                    raising hands to join your business
                  </div>
                </div>
                <form
                  className="form_wrapeer "
                  method="post"
                  name="userRegistrationForm"
                  onSubmit={this.submituserRegistrationForm}
                >
                  <div className="input-wrap">
                    <label className="label">
                      Your full name<span>*</span>
                    </label>
                    <input
                      type="text"
                      name="username"
                      autoComplete="false"
                      value={this.state.fields.username}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.username}
                    </span>
                  </div>
                  <div className="input-wrap">
                    <label className="label">
                      Email address<span>*</span>
                    </label>
                    <input
                      type="text"
                      autoComplete="false"
                      name="emailid"
                      value={this.state.fields.emailid}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.emailid}
                    </span>
                  </div>
                  <div className="input-wrap">
                    <label className="label">
                      Password<span>*</span>
                    </label>
                    <input
                      type="password"
                      name="password"
                      autoComplete="false"
                      value={this.state.fields.password}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.password}
                    </span>
                  </div>
                  <div className="term">
                    <input
                      type="checkbox"
                      id="term"
                      name="vehicle2"
                      value="Car"
                    />
                    <label for="term">
                      I agree to the Terms and Conditions
                    </label>
                  </div>
                  <div className="error-message">{this.state.message}</div>
                  <div className="input-wrap">
                    <button type="submit" className="button">
                      {this.state.loading ? "Signing up..." : "Sign up"}
                    </button>
                  </div>
                </form>

                {/* <div className="divider">
                  <span className="line" />
                  <span className="or">OR</span>
                  <span className="line" />
                </div>
                <div className="g-btn" onClick={() => this.signUpWithGoogle()}>
                  <img src={Google} alt="" />
                  {this.state.googleLoading
                    ? "Signing up with Google..."
                    : "Signup with Google"}
                  </div> */}
              </Col>
            </Row>
            <Row justify="center">
              <Col sm={12}>
                <div className="login-footer">
                  <div className="heading">
                    Grow your network marketing business today!
                  </div>
                  <div className="subheading">
                    We’re committed to your privacy. Loopin uses the information
                    you provide to us to contact you about our relevant content,
                    products, and services. You may unsubscribe from these
                    communications at any time. For more information, check out
                    our Privacy policy.
                  </div>
                  <div className="line-divider" />
                  <div className="copy">
                    ©2019 Loopin, Inc. All Rights Reserved.
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
    );
  }
}

export default Signup;
