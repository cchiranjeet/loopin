/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import "swiper/css/swiper.css";
// import * as firebase from "firebase";
// import "firebase/auth";
import { Container, Row, Col } from "react-grid-system";
import * as firebase from "firebase";
import "firebase/auth";
// import { Link } from "react-router-dom";
// import Google from "../../assets/img/google.jpg";
// import scrollToComponent from "react-scroll-to-component";
// import { ReactComponent as Share } from "../../assets/img/Share.svg";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        emailid: "",
        password: "",
        confirm_password: ""
      },
      errors: {},
      loading: false,
      googleLoading: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
    props.setShowSection(false);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm(e) {
    e.preventDefault();
    this.validateForm(() => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        firebase
          .auth()
          .sendPasswordResetEmail(this.state.fields.emailid)
          .then(() => {
            this.props.history.push("/signin");
          });
      }
    });
  }

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };

  validatePassword = cb => {
    const {
      fields: { password }
    } = this.state;
    if (password.length === 0) {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            password: "*Please enter your Password"
          }
        },
        cb
      );
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            password: undefined
          }
        },
        cb
      );
    }
  };

  validateForm = cb => {
    this.validatePassword(() => this.validateEmailID(cb));
  };
  render() {
    return (
      <div>
        <Container fluid>
          <div className="login-container">
            <Row>
              <Col sm={4} xs={4}>
                <div className="skip" onClick={this.props.history.goBack}>
                  Go Back
                </div>
              </Col>
              {/* <Col sm={8} xs={8}>
                <div className="login-title">
                  Don't have account? <Link to="/signup">Register</Link>
                </div>
              </Col> */}
            </Row>
            <Row justify="center">
              <Col sm={6}>
                <div className="login-wrapper">
                  <div className="header">Reset your password</div>
                  {/* <div className="subheader">
                    Create a free account today <br />& find people who are
                    raising hands to join your business
                  </div> */}

                  <form
                    className="form_wrapeer "
                    method="post"
                    name="userRegistrationForm"
                    onSubmit={this.submituserRegistrationForm}
                  >
                    <div className="input-wrap">
                      <label className="label">
                        Email address<span>*</span>
                      </label>
                      <input
                        type="text"
                        autoComplete="false"
                        name="emailid"
                        value={this.state.fields.emailid}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.emailid}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <label className="label">
                        Password<span>*</span>
                      </label>
                      <input
                        type="password"
                        name="password"
                        autoComplete="false"
                        value={this.state.fields.password}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.password}
                      </span>
                    </div>

                    <div className="input-wrap">
                      <label className="label">
                        Confirm Password<span>*</span>
                      </label>
                      <input
                        type="password"
                        name="confirm_password"
                        autoComplete="false"
                        value={this.state.fields.confirm_password}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.confirm_password}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <button type="submit" className="button">
                        continue
                      </button>
                    </div>
                  </form>
                </div>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
    );
  }
}

export default ForgotPassword;
