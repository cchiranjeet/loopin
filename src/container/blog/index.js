/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";
import * as firebase from "firebase";
import "firebase/database";
// Required for side-effects
import "firebase/firestore";
import HTML from "react-html-parser";
import Loader from "../../component/loader";
import blogImages from "../../assets/img/blog";
import { initialize, isInitialized } from "../../index";
const queryString = require('query-string');

class Blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
      total_pages: 2,
      pageStart: 0,
      pageEnd: 5,
      blog_list: null,
      loading: false
    };

    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    if (!isInitialized()) {
      initialize();
    }
    const queryStr = queryString.parse(this.props.location.search),
      currentPage = queryStr.page ? queryStr.page - 1 : 0;
    this.setState({ loading: true, currentPage })
    // firebase
    //   .database()
    //   .ref("/blogs")
    //   .once("value")
    //   .then(snapshot => {
    //     this.setState({ blog_list: snapshot.val(), loading: false });
    //   });

    firebase.firestore().collection("blogs").orderBy("id").get().then((snapshot) => {
      this.setState({ blog_list: snapshot.docs.map(doc => doc.data()), loading: false });
    });

  }
  // pageChange = x => {
  //   this.setState(
  //     {
  //       currentPage: x.selected
  //     },
  //     () => {
  //       this.getProperties();
  //     }
  //   );
  // };
  OpenBlogsDetailsPgae = (id, heading) => {
    this.props.history.push({
      pathname: `/blog/${heading}/${id}`,
      blog_list: this.state.blog_list
    });
  };

  handlePageClick = data => {
    const page = data.selected + 1;
    let queryStr = queryString.parse(this.props.location.search);
    queryStr['page'] = page;
    this.setState({ currentPage: data.selected }, () => {
      window.scrollTo(0, document.getElementById('blogContainer').offsetTop - document.getElementsByClassName('header-component')[0].offsetHeight);
    });
    this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(queryStr)}`);
  };

  render() {
    return (
      <>
        <Loader show={this.state.loading} />
        <div className="lopin-container">
          <div className="banner blog">
            <div className="banner-content">
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Col sm="12">
                    <div className="heading">Blogs</div>
                    <div className="subtitle">
                      Our sole purpose is to help you find compelling ideas,
                    <br />
                    knowledge, and perspectives.
                  </div>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
          <div className="blog-container" id="blogContainer">
            <Container fluid style={{ maxWidth: "992px" }}>
              <Row>
                {this.state.blog_list &&
                  this.state.blog_list.slice(this.state.currentPage === 0 ? 0 : 10, this.state.currentPage === 0 ? 10 : 13).map((blog) => {
                    return (
                      <Col sm={6}>
                        <Link
                          to={`/blog/${blog.heading.replace(/ /g, "-").replace(/[\?]/g, "")}/${blog.id}`}
                          target="_blank"
                        // onClick={() =>
                        //   this.OpenBlogsDetailsPgae(
                        //     id,
                        //     blog.heading.replace(/ /g, "-")
                        //   )
                        // }
                        >
                          <div className="blog">
                            <div
                              className="blog-image"
                              style={{
                                backgroundImage: `URL(${blogImages[blog.id + '.jpg'] || blog.imageUrl})`
                              }}
                            />
                            <div className="blog-content">
                              <div className="blog-date">
                                {blog.date}
                              </div>
                              <div
                                className="blog-heading"
                                style={{
                                  webkitBoxOrient: "vertical"
                                }}
                              >
                                {blog.heading}
                              </div>
                              <div
                                className="blog-subheading"
                                style={{
                                  height: "100px",
                                  overflow: "hidden",
                                  webkitBoxOrient: "vertical"
                                }}
                              >
                                {HTML(blog.content)}
                              </div>
                            </div>
                          </div>
                        </Link>
                      </Col>
                    );
                  })}
              </Row>
              <Row justify="center">
                <Col sm={12} md={12}>
                  {/* {this.state.properties.length > 0 && ( */}
                  <ReactPaginate
                    forcePage={this.state.currentPage}
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.total_pages}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}
                  />

                  {/* )} */}
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </>
    );
  }
}

export default Blog;
