/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";
import { Link } from "react-router-dom";
import * as firebase from "firebase";
import "firebase/database";
import "firebase/firestore";
import HTML from "react-html-parser";
import { initialize, isInitialized } from "../../index";
import Loader from '../../component/loader';
import blogImages from "../../assets/img/blog";

class Blogs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        username: "",
        emailid: "",
        message: ""
      },
      loading: false,
      errors: {},
      blog: null
    };

    props.setShowSection(true);
    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    if (!isInitialized()) {
      initialize();
    }
    this.setState({ loading: true });
    // firebase
    //   .database()
    //   .ref(`/blogs/${this.props.match.params.id}`)
    //   .once("value")
    //   .then(snapshot => {
    //     this.setState({ blog: snapshot.val(), loading: false });
    //   });

    firebase.firestore().collection("blogs").where("id", "==", parseInt(this.props.match.params.id)).get().then((snapshot) => {
      console.log(snapshot.docs[0].data());
      this.setState({ blog: snapshot.docs[0].data(), loading: false });
    });
  }
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm(e) {
    e.preventDefault();
    this.validateForm(() => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        this.props.updateSuccessState(true);

        const txt = Object.keys(this.state.fields).reduce(
          (acc, x) => `${acc} <br> ${x}: ${this.state.fields[x]}`,
          ""
        );

        console.log(txt);
      }
    });
  }

  validateUserName = cb => {
    const {
      fields: { username }
    } = this.state;

    if (username.length !== 0) {
      if (!username.match(/^[a-zA-Z ]*$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: "*Please enter alphabet characters only."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            username: "*Please enter your full name"
          }
        },
        cb
      );
    }
  };

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };

  validateMessage = cb => {
    const {
      fields: { message }
    } = this.state;
    if (message.length === 0) {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            message: "*Please enter your message."
          }
        },
        cb
      );
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            message: undefined
          }
        },
        cb
      );
    }
  };
  validateForm = cb => {
    this.validateMessage(() =>
      this.validateEmailID(() => this.validateUserName(cb))
    );
  };
  render() {
    // console.log(props);
    // const blog = this.props.TopCitiesBlog[
    //   this.props.match.params.category
    // ].cities.find(x => x.location_name === this.props.match.params.id);
    return (
      <>
        <Loader show={this.state.loading} />
        <div className="lopin-container">
          {!this.state.loading && this.state.blog &&
            <div className="banner banner3" style={{ backgroundImage: blogImages[this.state.blog.id + '.jpg'] ? `URL(${blogImages[this.state.blog.id + '.jpg']})` : undefined }}></div>
          }
          <div className="blog-container">

            {!this.state.loading &&
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Col sm={12}>
                    {this.state.blog && (
                      <div className="blog blog-details">
                        <div className="blog-content">
                          <div className="blog-date">{this.state.blog.date}</div>
                          <div className="blog-heading">
                            {this.state.blog.heading}
                          </div>
                          <div className="blog-subheading">
                            {HTML(this.state.blog.content)}
                          </div>
                        </div>
                      </div>
                    )}
                  </Col>

                  {/* <Col sm={5}>
                  <div className="latest-news">
                    <div className="news-heading">Latest News</div>
                    <div className="news">
                      <div className="news-image" />
                      <div className="news-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor{" "}
                      </div>
                    </div>
                    <div className="news">
                      <div className="news-image" />
                      <div className="news-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod tempor{" "}
                      </div>
                    </div>
                    <div className="news">
                      <div className="news-image" />
                      <div className="news-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor{" "}
                      </div>
                    </div>
                    <div className="news">
                      <div className="news-image" />
                      <div className="news-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor{" "}
                      </div>
                    </div>
                    <div className="news">
                      <div className="news-image" />
                      <div className="news-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor{" "}
                      </div>
                    </div>
                  </div>
                </Col> */}
                </Row>
              </Container>
            }
          </div>
          <div className="massage-container blog">
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row justify="end">
                <Col sm={6}>
                  <div className="comment-heading">Leave a comment here</div>
                  <div className="comment-subheading">
                    Your email address will not be published. Required fields are
                    marked *
                </div>
                  <form
                    className="form_wrapeer "
                    method="post"
                    name="userRegistrationForm"
                    onSubmit={this.submituserRegistrationForm}
                  >
                    <div className="input-wrap">
                      <input
                        type="text"
                        name="username"
                        placeholder="Name"
                        autoComplete="false"
                        value={this.state.fields.username}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.username}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <input
                        type="text"
                        autoComplete="false"
                        placeholder="Email"
                        name="emailid"
                        value={this.state.fields.emailid}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.emailid}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <textarea
                        name="message"
                        onChange={this.handleChange}
                        placeholder="Message"
                        value={this.state.fields.message}
                      />
                      <span className="errorMsg error">
                        {this.state.errors.message}
                      </span>
                    </div>

                    <div className="input-wrap">
                      <button type="submit" className="button">
                        Submit
                    </button>
                    </div>
                  </form>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </>
    );
  }
}

export default Blogs;
