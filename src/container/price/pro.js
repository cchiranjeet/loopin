import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";
import { toast } from 'react-toastify';
import Loader from '../../component/loader';

class ProPricing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        username: props.loggedIn?.displayName || '',
        emailid: props.loggedIn?.email || '',
        mobileno: props.loggedIn?.phoneNumber || ''
      },
      errors: {},
      loading: false
    };

    props.setShowSection(true);
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  handleChange = (e) => {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm = async (e) => {
    e.preventDefault();
    this.validateForm(async () => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        this.setState({ loading: true });
        try {
          // SEND EMAIL
          await fetch(`${process.env.REACT_APP_FIREBASE_API_ENDPOINT}/sendEmail`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              to: process.env.REACT_APP_SALES_EMAIL,
              subject: 'Loopin Pro & Enterprise Plan Enquiry',
              html: `<ul>
                <li>Name: ${this.state.fields.username}</li>
                <li>Email ID: ${this.state.fields.emailid}</li>
                <li>Mobile Number: ${this.state.fields.mobileno}</li>
              </ul>`
            })
          });
          this.setState({
            fields: {
              username: '',
              emailid: '',
              mobileno: ''
            }
          });
          toast.success(`Thank you, your request has been forwarded to our sales team, they will get back to you soon.`);
        } catch (err) { } finally {
          this.setState({ loading: false });
        }
      }
    });
  }

  validateUserName = cb => {
    const {
      fields: { username }
    } = this.state;

    if (username.length !== 0) {
      if (!username.match(/^[a-zA-Z ]*$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: "*Please enter alphabet characters only."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            username: "*Please enter your full name"
          }
        },
        cb
      );
    }
  };

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };
  validateMobileNo = cb => {
    const {
      fields: { mobileno }
    } = this.state;

    if (mobileno.length !== 0) {
      if (!mobileno.match(/^[0-9]{10}$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              mobileno: "*Please enter valid mobile no."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              mobileno: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            mobileno: "*Please enter your mobile no."
          }
        },
        cb
      );
    }
  };
  validateForm = cb => {
    this.validateMobileNo(() =>
      this.validateEmailID(() => this.validateUserName(cb))
    );
  };

  render() {
    return (
      <>
        <Loader show={this.state.loading} />
        <div className="lopin-container">
          <div className="content-details">
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col sm={5} offset={{ sm: 3.5 }}>
                  <div className="content-wrapper">
                    <form
                      className="form_wrapeer purchase-form"
                      method="post"
                      name="userRegistrationForm"
                      onSubmit={this.submituserRegistrationForm}
                    >
                      <div className="heading">
                        <strong>Loopin Pro</strong><br />Advance features for growing teams.
                    <br />
                        <br />
                        <strong>Loopin Enterprise</strong><br />Scalable solutions for brands.
                  </div>
                      <div className="input-wrap">
                        <input
                          type="text"
                          name="username"
                          placeholder="Name"
                          autoComplete="false"
                          value={this.state.fields.username}
                          onChange={this.handleChange}
                        />
                        <span className="errorMsg">
                          {this.state.errors.username}
                        </span>
                      </div>
                      <div className="input-wrap">
                        <input
                          type="text"
                          autoComplete="false"
                          placeholder="Email"
                          name="emailid"
                          value={this.state.fields.emailid}
                          onChange={this.handleChange}
                        />
                        <span className="errorMsg">
                          {this.state.errors.emailid}
                        </span>
                      </div>
                      <div className="input-wrap">
                        <input
                          type="text"
                          name="mobileno"
                          placeholder="Contact number"
                          value={this.state.fields.mobileno}
                          onChange={this.handleChange}
                        />
                        <span className="errorMsg">
                          {this.state.errors.mobileno}
                        </span>
                      </div>
                      <div className="input-wrap">
                        <button type="submit" className="button">
                          Contact Sales
                  </button>
                      </div>
                    </form>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </>
    );
  }
}

export default ProPricing;
