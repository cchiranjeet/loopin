/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-grid-system";
import { usePurchase } from "../../component/purchase";
import Loader from '../../component/loader';
// import { Container, Row, Col } from "react-grid-system";
// import { Navigation, Pagination } from "swiper/js/swiper.esm";
// import Video from "../../assets/img/SampleVideo.mp4";
// import banner1 from "../../assets/img/banner1.jpg";
// import Popup from "../../assets/img/popup.jpg";

// import scrollToComponent from "react-scroll-to-component";
// import { ReactComponent as Share } from "../../assets/img/Share.svg";

class Pricing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        username: props.loggedIn?.displayName || '',
        emailid: props.loggedIn?.email || '',
        mobileno: props.loggedIn?.phoneNumber || ''
      },
      addedPlan: props.cartValue.map(x => x.name),
      errors: {},
      loading: false
    };
    props.setShowSection(true);
    this.purchase = usePurchase({ setLoading: this.setLoading });
  }

  setLoading = (loading) => {
    this.setState({ loading: loading });
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  handleChange = (e) => {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  static getDerivedStateFromProps(props, state) {
    return {
      addedPlan: props.cartValue.map(x => x.name)
    }
  }

  submitForm = async (e) => {
    e.preventDefault();
    this.validateForm(async () => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        this.setState({ loading: true });
        try {
          const purchaseResponse = await this.purchase.purchaseBot({
            email: this.state.fields.emailid,
            username: this.state.fields.username,
            mobileno: this.state.fields.mobileno,
            user: this.props.loggedIn,
            plan: 'basic'
          });
          this.props.history.push('/thank-you');
        } catch (err) { } finally {
          this.setState({ loading: false });
        }
      }
    });
  }

  validateUserName = cb => {
    const {
      fields: { username }
    } = this.state;

    if (username.length !== 0) {
      if (!username.match(/^[a-zA-Z ]*$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: "*Please enter alphabet characters only."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            username: "*Please enter your full name"
          }
        },
        cb
      );
    }
  };
  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };
  validateMobileNo = cb => {
    const {
      fields: { mobileno }
    } = this.state;

    if (mobileno.length !== 0) {
      if (!mobileno.match(/^[0-9]{10}$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              mobileno: "*Please enter valid mobile no."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              mobileno: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            mobileno: "*Please enter your mobile no."
          }
        },
        cb
      );
    }
  };
  validateForm = cb => {
    this.validateMobileNo(() =>
      this.validateEmailID(() => this.validateUserName(cb))
    );
  };

  render() {
    return (
      <>
        <Loader show={this.state.loading} />
        <div className="pricing-container">
          <div className="massage-container home">
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col xs={12} className="text-center">
                  <div className="heading text-center">
                    For PRO and Enterprise plans connect with us?
                </div>
                  <Link to="/price-plan/pro-enterprise" className="button d-inline-block">
                    Connect now
                </Link>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </>
    );
  }
}

export default Pricing;
