<div className="pricing-container">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={12}>
                <div className="price-heading">Plans and Pricing</div>
              </Col>
              <Col sm={4} offset={{ sm: 2 }}>
                <div class="pricing-table clearfix">
                  <ul class="highlight-high full-w">
                    <li class="heading">BASIC</li>
                    <li class="price"><strike>₹30,000</strike> ₹19,997</li>
                    <li class="feature">
                      <span>
                        Must-have features for the new-age network marketers.
                    </span>
                    </li>
                    <li class="feature">
                      <span>1 Profiles</span>
                    </li>
                    <li class="feature">
                      <span>GETTING STARTED</span>
                    </li>
                    <li class="feature">
                      <span>Everything in Basic</span>
                    </li>
                    <li class="feature">
                      <span>
                        Find, qualify, and connect with people who are super
                        interested in your business.
                    </span>
                    </li>
                    <li class="feature">
                      <span>Get all your prospects in one place</span>
                    </li>
                    <li class="feature">
                      <span>
                        Monitor your prospects and stay on top of their journey
                    </span>
                    </li>
                    {/* <li class="feature">
                    <span>
                      Send unlimited voice and text messages to your prospects
                    </span>
                  </li> */}
                    <li class="feature">
                      <span>
                        Create your personalised DigitCard for prospecting
                    </span>
                    </li>
                    <li class="feature">
                      <span>
                        Select among 5 pre-made landing pages templates designed for your prospects
                    </span>
                    </li>
                    <li class="feature">
                      <span>
                        Find everything at one place – prospect search, recent 20
                    </span>
                    </li>
                    <li class="call-to-action">
                      {/* <span
                        onClick={() => {
                          if (this.state.addedPlan === 'Basic Plan') {
                            return;
                          }
                          this.props.setCartValue(prevState => [
                            ...prevState,
                            {
                              name: "Basic Plan",
                              price: 1666,
                              quantity: 1
                            }
                          ]);
                        }}
                      >
                        {this.state.addedPlan.includes('Basic Plan')
                          ? "Buy Now"
                          : "Buy Now"}
                      </span> */}
                    </li>
                  </ul>
                  {/* <ul class="highlight-high">
                  <li class="heading">PRO</li>
                  <li class="price">₹ 4000 / Monthly</li>
                  <li class="feature">
                    <span>
                      Advanced features for pros who need more advanced.
                    </span>
                  </li>
                  <li class="feature">
                    <span>50 Profile</span>
                  </li>
                  <li class="feature">
                    <span>GROWING BRANDS</span>
                  </li>
                  <li class="feature">
                    <span>Everything in Pro</span>
                  </li>
                  <li class="feature">
                    <span>
                      Find, qualify, and connect with people who are super
                      interested in your business.
                    </span>
                  </li>
                  <li class="feature">
                    <span>Get all your prospects in one place</span>
                  </li>
                  <li class="feature">
                    <span>
                      Monitor your prospects and stay on top of their journey
                    </span>
                  </li>
                  <li class="feature">
                    <span>
                      Send unlimited voice and text messages to your prospects
                    </span>
                  </li>
                  <li class="feature">
                    <span>
                      Find everything at one place – prospect search, recent 20
                    </span>
                  </li>
                  <li class="call-to-action">
                    <span
                      onClick={() => {
                        if (this.state.addedPlan.includes('Pro Plan')) {
                          return;
                        }
                        this.props.setCartValue(prevState => [
                          ...prevState,
                          {
                            name: "Pro Plan",
                            price: 4000,
                            quantity: 1
                          }
                        ]);
                      }}
                    >
                      {this.state.addedPlan.includes('Pro Plan')
                        ? "Added to cart!"
                        : "Add to cart"}
                    </span>
                  </li>
                </ul>
                <ul class="highlight-medium">
                  <li class="heading">ENTERPRISE</li>
                  <li class="price">₹ 10000 / Monthly</li>

                  <li class="feature">
                    <span>
                      Customized features for brands that want more customers.
                    </span>
                  </li>
                  <li class="feature">
                    <span>1000 Profile</span>
                  </li>
                  <li class="feature">
                    <span>LARGER TEAMS</span>
                  </li>
                  <li class="feature">
                    <span>Everything in Enterprise</span>
                  </li>
                  <li class="feature">
                    <span>
                      Find, qualify, and connect with people who are super
                      interested in your business.
                    </span>
                  </li>
                  <li class="feature">
                    <span>Get all your prospects in one place</span>
                  </li>
                  <li class="feature">
                    <span>
                      Monitor your prospects and stay on top of their journey
                    </span>
                  </li>
                  <li class="feature">
                    <span>
                      Send unlimited voice and text messages to your prospects
                    </span>
                  </li>
                  <li class="feature">
                    <span>
                      Find everything at one place – prospect search, recent 20
                    </span>
                  </li>
                  <li class="call-to-action">
                    <span
                      onClick={() => {
                        if (this.state.addedPlan.includes('Enterprise Plan')) {
                          return;
                        }
                        this.props.setCartValue(prevState => [
                          ...prevState,
                          {
                            name: "Enterprise Plan",
                            price: 10000,
                            quantity: 1
                          }
                        ]);
                      }}
                    >
                      {this.state.addedPlan.includes('Enterprise Plan')
                        ? "Added to cart!"
                        : "Add to cart"}
                    </span>
                  </li>
                </ul> */}
                </div>
              </Col>
              <Col sm={4}>
                <form
                  className="form_wrapeer purchase-form"
                  method="post"
                  name="userRegistrationForm"
                  onSubmit={this.submitForm}
                >
                  <div className="heading">
                    Be a digital network marketer
                  </div>
                  <div className="input-wrap">
                    <input
                      type="text"
                      name="username"
                      placeholder="Name"
                      autoComplete="false"
                      value={this.state.fields.username}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.username}
                    </span>
                  </div>
                  <div className="input-wrap">
                    <input
                      type="text"
                      autoComplete="false"
                      placeholder="Email"
                      name="emailid"
                      value={this.state.fields.emailid}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.emailid}
                    </span>
                  </div>
                  <div className="input-wrap">
                    <input
                      type="text"
                      name="mobileno"
                      placeholder="Contact number"
                      value={this.state.fields.mobileno}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.mobileno}
                    </span>
                  </div>
                  <div className="input-wrap">
                    <button type="submit" className="button">
                      Pre Register
                    </button>
                  </div>
                </form>
              </Col>
            </Row>
          </Container>
        </div>