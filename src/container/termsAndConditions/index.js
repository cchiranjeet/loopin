/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";

class Faq extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="faq">
        <Container fluid style={{ maxWidth: "1200px" }}>
          <Row>
            <Col sm={12}>
              <div className="heading">Terms &amp; Conditions</div>
              <div className="subheading">
                Loopin IT Innovations, LLP Terms of Service
              </div>
              <div className="wrapper">
                <div className="question">
                  Terms
                </div>
                <div className="answer">
                  By accessing the website at http://loopinglobal.com, you are agreeing to be bound by these
                  terms of service, all applicable laws and regulations, and agree that you are responsible for
                  compliance with any applicable local laws. If you do not agree with any of these terms, you
                  are prohibited from using or accessing this site. The materials contained in this website are
                  protected by applicable copyright and trademark law.
                </div>
                <div className="question">
                  Use License
                </div>
                <div className="answer">
                  Permission is granted to temporarily download one copy of the materials (information or
                  software) on Loopin IT Innovations, LLP&#39;s website for personal, non-commercial transitory
                  viewing only. This is the grant of a license, not a transfer of title, and under this license you
                  may not:
                  <br />
                  <br />
                  <ol>
                    <li>
                      modify or copy the materials;
                    </li>
                    <li>
                      use the materials for any commercial purpose, or for any public display (commercial or non-commercial);
                    </li>
                    <li>
                      attempt to decompile or reverse engineer any software contained on Loopin IT Innovations, LLP&#39;s website;
                    </li>
                    <li>
                      remove any copyright or other proprietary notations from the materials; or
                    </li>
                    <li>
                      transfer the materials to another person or &quot;mirror&quot; the materials on any other server.
                    </li>
                  </ol>
                  <br />
                  This license shall automatically terminate if you violate any of these restrictions and may be
                  terminated by Loopin IT Innovations, LLP at any time. Upon terminating your viewing of
                  these materials or upon the termination of this license, you must destroy any downloaded
                  materials in your possession whether in electronic or printed format.
                </div>
                <div className="question">
                  Disclaimer
                </div>
                <div className="answer">
                  The materials on Loopin IT Innovations, LLP&#39;s website are provided on an &#39;as is&#39; basis.
                  Loopin IT Innovations, LLP makes no warranties, expressed or implied, and hereby
                  disclaims and negates all other warranties including, without limitation, implied warranties or
                  conditions of merchantability, fitness for a particular purpose, or non-infringement of
                  intellectual property or other violation of rights.
                  <br />
                  Further, Loopin IT Innovations, LLP does not warrant or make any representations
                  concerning the accuracy, likely results, or reliability of the use of the materials on its website
                  or otherwise relating to such materials or on any sites linked to this site.
                </div>
                <div className="question">Limitations</div>
                <div className="answer">
                  In no event shall Loopin IT Innovations, LLP or its suppliers be liable for any damages
                  (including, without limitation, damages for loss of data or profit, or due to business
                  interruption) arising out of the use or inability to use the materials on Loopin IT Innovations,
                  <br />
                  LLP&#39;s website, even if Loopin IT Innovations, LLP or a Loopin IT Innovations, LLP authorized
                  representative has been notified orally or in writing of the possibility of such damage.
                  Because some jurisdictions do not allow limitations on implied warranties, or limitations of
                  liability for consequential or incidental damages, these limitations may not apply to you.
                </div>
                <div className="question">Accuracy of materials</div>
                <div className="answer">
                  The materials appearing on Loopin IT Innovations, LLP&#39;s website could include technical,
                  typographical, or photographic errors. Loopin IT Innovations, LLP does not warrant that any
                  of the materials on its website are accurate, complete or current. Loopin IT Innovations, LLP
                  may make changes to the materials contained on its website at any time without notice.
                  However Loopin IT Innovations, LLP does not make any commitment to update the
                  materials.
                </div>
                <div className="question">
                    Links
                </div>
                <div className="answer">
                  Loopin IT Innovations, LLP has not reviewed all of the sites linked to its website and is not
                  responsible for the contents of any such linked site. The inclusion of any link does not imply
                  endorsement by Loopin IT Innovations, LLP of the site. Use of any such linked website is at
                  the user&#39;s own risk.
                </div>
                <div className="question">Modifications</div>
                <div className="answer">
                  Loopin IT Innovations, LLP may revise these terms of service for its website at any time
                  without notice. By using this website you are agreeing to be bound by the then current
                  version of these terms of service.
                </div>

                <div className="question">
                  Governing Law
                </div>
                <div className="answer">
                  These terms and conditions are governed by and construed in accordance with the laws of
                  Haryana, India and you irrevocably submit to the exclusive jurisdiction of the courts in that
                  State or location.
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Faq;
