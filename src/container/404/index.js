import React from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-grid-system";

class Errorpage extends React.Component {
  constructor(props) {
    super(props);
    props.setShowSection(true);
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <div className="wrapper error">
        <div className="heading">ERROR 404</div>
        <div className="subheading">Oh uh! You are out of this world.</div>
        <Link to="/" className="goback">
          Go to Home page
        </Link>
        <div className="image-container">
          <div className="image-first" />
        </div>
      </div>
    );
  }
}

export default Errorpage;
