/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
// import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-grid-system";
import banner1 from "../../assets/img/coursebanner.jpg";
import Popup from "../../component/modal";
import Loader from '../../component/loader';
// import Popup from "../../assets/img/bookpopup.jpg";
// import { Navigation, Pagination } from "swiper/js/swiper.esm";
// import Video from "../../assets/img/SampleVideo.mp4";
// import Image from "../../assets/img/testimonioal.jpg";

// import scrollToComponent from "react-scroll-to-component";
// import { ReactComponent as Share } from "../../assets/img/Share.svg";
import { usePurchase } from "../../component/purchase";
import { initialize, isInitialized } from "../../index";
const coursePrice = '2,497',
  thinkificCourseId = 608787,
  thinkificProductId = 641268,
  courseName = 'Success Blueprint Course';
class Course extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        username: props.loggedIn?.displayName || '',
        emailid: props.loggedIn?.email || '',
        mobileno: props.loggedIn?.phoneNumber || ''
      },
      showPopup: false,
      errors: {},
      loading: false
    };
    this.handleChange = this.handleChange.bind(this);
    props.setShowSection(true);
    this.purchase = usePurchase({setLoading: this.setLoading});
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    if (!isInitialized()) {
      initialize();
    }
  }

  setLoading = (loading) => {
    this.setState({loading: loading});
  }

  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm = async (e) => {
    e.preventDefault();
    this.validateForm(async () => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {

        // this.props.updateSuccessState(true);

        // const txt = Object.keys(this.state.fields).reduce(
        //   (acc, x) => `${acc} <br> ${x}: ${this.state.fields[x]}`,
        //   ""
        // );

        // console.log(txt);
        this.setState({ loading: true });
        try {
          const purchaseResponse = await this.purchase.purchaseCourse({
            email: this.state.fields.emailid,
            username: this.state.fields.username,
            user: this.props.loggedIn,
            courseName,
            thinkificCourseId,
            thinkificProductId
          });
          // SEND CONFIMATION EMAIL
          await fetch(`${process.env.REACT_APP_FIREBASE_API_ENDPOINT}/trainingPayment`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify(purchaseResponse.confirmEmailParams)
          });
          this.props.history.push(this.props.location.pathname + '/thank-you');
        } catch (err) { } finally {
          this.setState({ loading: false });
        }
      }
    });
  }

  // createThinkificUser = (user) => {
  //   return fetch("https://api.thinkific.com/api/public/v1/users", {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //       "X-Auth-API-Key": process.env.REACT_APP_THINKIFIC_API_KEY,
  //       "X-Auth-Subdomain": process.env.REACT_APP_THINKIFIC_API_SUB_DOMAIN
  //     },
  //     body: JSON.stringify({
  //       first_name: user.first_name,
  //       last_name: user.last_name,
  //       email: user.email,
  //       password: user.password
  //     })
  //   });
  // }

  validateUserName = cb => {
    const {
      fields: { username }
    } = this.state;

    if (username.length !== 0) {
      if (!username.match(/^[a-zA-Z ]*$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: "*Please enter alphabet characters only."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            username: "*Please enter your full name"
          }
        },
        cb
      );
    }
  };

  OpenPopup = () => {
    this.setState({
      showPopup: true
    });
    document.body.style.position = "fixed";
    document.body.style.overflow = "Hidden";
  };
  closePopup = () => {
    this.setState({
      showPopup: false
    });
    document.body.style.position = "unset";
    document.body.style.overflow = "unset";
  };

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };
  validateMobileNo = cb => {
    const {
      fields: { mobileno }
    } = this.state;

    if (mobileno.length !== 0) {
      if (!mobileno.match(/^[0-9]{10}$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              mobileno: "*Please enter valid mobile no."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              mobileno: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            mobileno: "*Please enter your mobile no."
          }
        },
        cb
      );
    }
  };
  validateForm = cb => {
    this.validateMobileNo(() =>
      this.validateEmailID(() => this.validateUserName(cb))
    );
  };

  // getThinkificCourse = () => {
  //   fetch(`https://api.thinkific.com/api/public/v1/courses/${thinkificCourseId}`, {
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //       "X-Auth-API-Key": process.env.REACT_APP_THINKIFIC_API_KEY,
  //       "X-Auth-Subdomain": process.env.REACT_APP_THINKIFIC_API_SUB_DOMAIN
  //     }
  //   })
  //     .then(x => x.json())
  //     .then(
  //       response => console.log(response),
  //       response => console.log(response)
  //     );
  // };

  render() {
    return (
      <>
        <Loader show={this.state.loading} />
        <div className="lopin-container">
          <div className="banner course">
            {/* <img src={banner1} alt="banner" /> */}
            <div className="banner-content">
              <Container fluid style={{ maxWidth: "1200px" }}>
                <Row>
                  <Col sm="12">
                    <div className="heading course">
                      Start Networking your Dreams
                    <br /> & Marketing yourself with LOOPIN
                  </div>
                    <div className="subcontent">
                      We are changing the Indian way of network marketing.
                    <br /> Find, manage and connect with more prospects in less
                    than no time.
                  </div>
                    <a>
                      <div className="download" onClick={this.OpenPopup} >Watch Promo</div>
                    </a>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
          <div className="content-details">
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col sm={12}>
                  <div className="heading course">
                    UPGRADE YOUR METHODOLOGIES. MAXIMIZE YOUR <br />
                  POTENTIAL. OPEN INFINITE POSSIBILITIES.
                </div>
                  <div className="subtitle" id="form">
                    Master the new-age network marketing with Kamal Anjana..
                </div>
                </Col>
              </Row>
              <Row>
                <Col sm={7}>
                  <div className="content-wrapper">
                    <div className="title">The Mission</div>
                    <div className="view-content">
                      The network marketing industry is capable of much more than
                      its current state! The industry is home to infinite
                      possibilities and dreams. And all that you require to master
                      it is – a tactical skill and right guidance.
                    <br />
                      <br />
                    This Success Blueprint course by Kamal Aanjna encapsulates everything
                    you need to know to be successful in today’s increasingly
                    connected world. Whether you want to know about how to build
                    your social media profile, start a business, how to convert
                    a potential prospect or how to upgrade the conventional way
                    of network marketing to fit in today’s time and age– we can
                    help.
                    <br />
                      <br />
                    Our Success Blueprint course is designed to give you the consistent
                    help and support that you need on your path of achieving
                    success and fame.
                    <br />
                      <br />
                    So, while you help people live their best life through your
                    products and opportunities, we are here to make sure that
                    the journey is seamless for you.
                    <br />
                      <br /> What are you waiting for? Get learning today.
                  </div>
                  </div>
                </Col>
                <Col sm={5}>
                  <form
                    className="form_wrapeer purchase-form"
                    method="post"
                    name="userRegistrationForm"
                    onSubmit={this.submituserRegistrationForm}
                  >
                    <div className="offer">
                      Special Offer Price <strike>₹5,000</strike>
                      <span>₹{coursePrice}</span>
                    </div>
                    <div className="heading">
                      Get access to the secrets of the new-age network marketing
                  </div>
                    <div className="input-wrap">
                      <input
                        type="text"
                        name="username"
                        placeholder="Name"
                        autoComplete="false"
                        value={this.state.fields.username}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.username}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <input
                        type="text"
                        autoComplete="false"
                        placeholder="Email"
                        name="emailid"
                        value={this.state.fields.emailid}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.emailid}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <input
                        type="text"
                        name="mobileno"
                        placeholder="Contact number"
                        value={this.state.fields.mobileno}
                        onChange={this.handleChange}
                      />
                      <span className="errorMsg">
                        {this.state.errors.mobileno}
                      </span>
                    </div>
                    <div className="input-wrap">
                      <button type="submit" className="button">
                        Yes! I want this course
                    </button>
                    </div>
                  </form>
                </Col>
              </Row>
            </Container>
          </div>
          {this.state.showPopup && (
            <Popup
              text='Click "Close Button" to hide popup'
              closePopup={this.closePopup}
              videoType={'youtube'}
              videoId={'vFxN9DSZKBo'}
            // url="https://firebasestorage.googleapis.com/v0/b/loopin-fad04.appspot.com/o/Promo%20Training%20Course_ENGLISH%20Without%20Thank%20You.mp4?alt=media&token=57707632-bec2-44af-8fa3-59b2bfbe72dd"
            />
          )}
        </div>
      </>
    );
  }
}

export default Course;
