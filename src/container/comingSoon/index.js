import React, { Component, useEffect } from "react";
import { Container, Row, Col, Hidden, Visible } from "react-grid-system";
import comingSoonBg from "../../assets/img/coming-soon-bg.png";
import comingSoonMobileImage from "../../assets/img/coming-soon-mobile-image.png";
import useScript from "../../hooks/useScript";

const ComingSoon = props => {
  // LeadSquared Tracking Code
  // const leadSquaredTrackingScript = document.createElement('script');
  // useScript('http://web-in21.mxradon.com/t/Tracker.js', 'external', () => {
  //   const inlineScript = document.createTextNode("pidTracker('46572')");
  //   leadSquaredTrackingScript.appendChild(inlineScript);
  //   document.body.appendChild(leadSquaredTrackingScript);
  // });

  // useEffect(() => {
  //   return () => {
  //     document.body.removeChild(leadSquaredTrackingScript);
  //   }
  // }, []);

  return (
    <Container fluid style={{ position: "relative", maxWidth: "1800px", backgroundColor: "white", height: "100vh", backgroundRepeat: "no-repeat", backgroundPosition: "bottom", backgroundSize: "100%", backgroundImage: `url(${comingSoonBg})` }}>
      <Hidden>
        <div className="full-h full-w" style={{ position: "absolute", left: 0, backgroundRepeat: "no-repeat", backgroundPositionX: "right", backgroundPositionY: "bottom", backgroundSize: "contain", backgroundImage: `url(${comingSoonMobileImage})` }}></div>
      </Hidden>
      <Row className="full-h">
        <Visible md lg xl>
          <Col md={1}></Col>
        </Visible>
        <Col sm={12} md={5} className="v-align-content">
          <div className="full-h" style={{ display: "flex", flexDirection: "column" }}>
            <div style={{width: "90%", margin: "0 auto"}}>
              <h1 className="m-half--ends color-primary font-xl">COMING SOON</h1>
              <h1 className="font-weight-normal m-half--bottom">India's First Ever Network Marketing<br />Tool is on Its way</h1>
            </div>
            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSefqnjdWSMsznDFje0y7NKaxwj4WnbJUKccqK4DgfDpyFfd-w/viewform?embedded=true" frameborder="0" marginheight="0" marginwidth="0" style={{flex: 1}}>Loading…</iframe>
          </div>
        </Col>
      </Row>
    </Container>
  )
}

export default ComingSoon;