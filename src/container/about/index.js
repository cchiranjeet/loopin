/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";
// import { Link } from "react-router-dom";
import image1 from "../../assets/img/our-mission.jpg";
import image2 from "../../assets/img/our-plan.jpg";
import image3 from "../../assets/img/our-vision.jpg";
import AboutImg from "../../assets/img/aboutus.jpg";
import Value from "../../assets/img/value.jpg";

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="about-container">
        <div className="aboutus">
          While you help people live their best lives, <br />
          We are here to help you succeed.
        </div>
        <section id="about-first">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={7}>
                <div className="about-details">
                  <div className="heading">
                    The next generation of network marketing
                  </div>
                  <div className="subheading">
                    Loopin is committed to fostering the growth of every network
                    marketer out there because we know there’s a great potential
                    in each one of you. That’s why we’ve created a tool uniting
                    technology, learning and community to help you grow your
                    network better every day. As the world is getting more and
                    more digitalized, it is affecting every other industry and
                    is often credited with making their workflow more flexible
                    and concise. Then why should network marketing be any
                    different? We believe that LOOPIN is a perfect tool for
                    bringing in a digital revolution in the network marketing
                    industry.
                  </div>
                  <div className="bullets">
                    <div className="name">THE TOOL</div>
                    <div className="subheading">
                      Loopin is a light smartphone-friendly tool that perfectly
                      integrates seamlessly with your Facebook messenger (talk
                      about convenience). There is no additional application for
                      you to install, all you have to do is share a URL online.
                      There is also an offline sharing mode consisting of a QR
                      code that makes it a perfect tool to have around during
                      live events.
                    </div>
                    <div className="name"> A CUTTING EDGE APPROACH</div>
                    <div className="subheading">
                      This highly sophisticated tool has a built-in intelligence
                      in the form of SUPERBOT that can provide innovative
                      networking solutions. Loopin is you’re personalized
                      assistant that keeps tabs on the progress of your
                      prospects, further ensuring that you partner-up with
                      reliable people who can form an effective and rewarding
                      downline.
                    </div>
                  </div>
                </div>
              </Col>
              <Col sm={5}>
                <div className="image">
                  <img src={AboutImg} />
                </div>
              </Col>
            </Row>
          </Container>
        </section>
        <section id="about">
          <div className="container">
            <Container fluid style={{ maxWidth: "1200px" }}>
              <header className="section-header">
                <h3>About Us</h3>
                <p></p>
              </header>
            </Container>
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col sm={4}>
                  <div className="about-col">
                    <div className="img">
                      <img src={image1} alt="" className="img-fluid" />
                      <div className="icon">
                        <i className="ion-ios-speedometer-outline"></i>
                      </div>
                    </div>
                    <h2 className="title">
                      Our Mission
                    </h2>
                    <p>
                      Leveraging technology to simplify and better opportunities
                      that catalyze/promote human economic advancement and
                      personal growth.
                    </p>
                  </div>
                </Col>

                <Col sm={4}>
                  <div className="about-col">
                    <div className="img">
                      <img src={image2} alt="" className="img-fluid" />
                      <div className="icon">
                        <i className="ion-ios-list-outline"></i>
                      </div>
                    </div>
                    <h2 className="title">
                      Our Plan
                    </h2>
                    <p>
                      We encourage creativity and innovation. We continuously
                      strive for improvement, progress, and achievement of both
                      economic and personal goals of our partners.
                    </p>
                  </div>
                </Col>

                <Col sm={4}>
                  <div className="about-col">
                    <div className="img">
                      <img src={image3} alt="" className="img-fluid" />
                      <div className="icon">
                        <i className="ion-ios-eye-outline"></i>
                      </div>
                    </div>
                    <h2 className="title">
                      Our Vision
                    </h2>
                    <p>
                      Create entrepreneurial opportunities with our hassle-free
                      and easy product & services.
                    </p>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </section>
        <section id="about-first">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <header className="section-header">
              <h3>Our Values</h3>
              <p>
                At Loopin, we share a lot of basic beliefs – trustworthiness,
                honesty and regard for individuals – which support all the work
                we do.
              </p>
            </header>
          </Container>
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={5}>
                <div className="image">
                  <img src={Value} />
                </div>
              </Col>
              <Col sm={7}>
                <div className="about-details value">
                  <div className="bullets value">
                    <div className="name first">CREATIVITY AND INNOVATION:</div>
                    <div className="content">
                      We encourage creativity and innovation. We continuously
                      strive for improvement, progress, and achievement of both
                      economic and personal goals of our partners. Our
                      technologically advanced tools, products, and services
                      help people to hit the ground running smoothly.
                    </div>
                    <div className="name"> EMPOWERMENT OF PARTNERS:</div>
                    <div className="content">
                      We equip our partners to set their personal goals and
                      provide them with the right opportunities to make them
                      real. Our constant focus is on encouraging and empowering
                      our partners to explore opportunities that can help them
                      recognize their potential and benefit from it in the best
                      possible way.
                    </div>
                    <div className="name"> TRUST & CONFIDENCE:</div>
                    <div className="content">
                      We believe in the concept of partnership which is healthy,
                      active and builds on trust and confidence. We truly value
                      the partnership that exists between our business,
                      employees and partners. We promise to stand by them
                      whenever they need us. Each product and service is
                      designed in the best interest of our partners, which
                      accounts more trust and confidence.
                    </div>
                    <div className="name"> RESPECT & CREDIBILITY:</div>
                    <div className="content">
                      We believe that more than anything, integrity is most
                      important to our business. The success of our business is
                      not just measured in economic terms, but by the amount of
                      respect, trust and credibility we achieve.
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </div>
    );
  }
}

export default About;
