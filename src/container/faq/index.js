/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import { Container, Row, Col } from "react-grid-system";

class Faq extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    props.setShowSection(true);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="faq">
        <Container fluid style={{ maxWidth: "1200px" }}>
          <Row>
            <Col sm={12}>
              <div className="heading">How can we help you?</div>
              <div className="subheading">
                You can browse through the topics below to find the answers that
                you are looking for.
              </div>
              <div className="wrapper">
                <div className="question">
                  How can I do well in network marketing?
                </div>
                <div className="answer">
                  Network Marketing is a profession in which it’s the smart work
                  that counts rather than hard work. So, to do well in network
                  marketing you need to be a smart worker. To succeed in network
                  marketing you should plan your strategy well before. And to
                  form a successful strategy you first need to research about
                  your audience, know their interest, and focus on the problems
                  they are facing. By doing so, you will not only find a new
                  prospect but you can also sell your products.
                </div>
                <div className="question">
                  What is the fastest way to succeed in network marketing?
                </div>
                <div className="answer">
                  There are no shortcuts to success in anything you do. The
                  success in this business is directly proportional to how much
                  effort you put in. Though, having fair knowledge of digital
                  platforms can help you grow your business at a faster pace
                  than the old-school and conventional methods.
                  <br />
                  <br />
                  You can download Network Marketing 5.0 and experience the new
                  avatar of network marketing. The eBook is co-authored by
                  Sajeev Nair and Kamal Anjana and is a great book for readers
                  who want to navigate the new online workspace.
                </div>
                <div className="question">
                  What is network marketing and how does it work?
                </div>
                <div className="answer">
                  As the name itself suggests, network marketing is a profession
                  that focuses on both networking and marketing. It is similar
                  to the traditional marketing model of independent contractors
                  buying into a company and earning a commission on the products
                  they are selling. Network Marketing is a powerful way to move
                  products to consumers.
                </div>
                <div className="question">How do I find prospects?</div>
                <div className="answer">
                  There are two ways of finding prospects.
                  <ol>
                    <li>
                      The traditional way - This is a time-consuming way that
                      requires a lot of effort. If you are following the
                      traditional way a lot of your time will be wasted in
                      meeting people, attending events, etc.
                    </li>
                    <li>
                      The Digital Way - This is an efficient and quick way that
                      requires just a social media account and a working
                      internet connection. All you need to do is just engage
                      your Target Audience on social media.
                    </li>
                  </ol>
                </div>
                <div className="question">How do I recruit prospects?</div>
                <div className="answer">
                  Recruiting prospects using the traditional method can be a
                  difficult task. However, certain tools are available online
                  that you can use to ease the recruiting process. Our product
                  LOOPIN is a great tool that will not only help you in
                  recruiting but also in keeping track of your prospects. It
                  will arrange your list as per their activity. Thus, showing
                  you the ones that are highly active and motivated. When you
                  know which one of your lead is motivated, you can decide and
                  pick a lead that could prove fruitful for your venture.
                </div>
                <div className="question">
                  Is network marketing industry a scam?
                </div>
                <div className="answer">
                  Irrespective of the recent negative sentiments shared by the
                  community towards network marketing, this industry is as
                  genuine as any other. The network marketing business is a real
                  business where you share opportunities, and products that
                  excite you with their genuine quality. To be precise, network
                  marketing is a conversational distribution system - where you
                  just have to talk to people and share your genuinely positive
                  experiences.
                </div>
                <div className="question">Do I have to quit my job?</div>
                <div className="answer">
                  Not at all. Network marketing is a perfect profession for
                  people who are looking for some passive income. You can start
                  your network marketing business without even affecting your
                  current job. Moreover, you can continue to do it as a
                  part-time business for an indefinite period.  You must know
                  that there are nearly 90% of people who are doing network
                  marketing as their part-time and they’ve become extremely
                  successful.
                </div>

                <div className="question">
                  How much will I earn through network marketing?
                </div>
                <div className="answer">
                  Well, even we don’t know the answer to that. The amount of
                  money you can earn through network marketing is directly
                  proportional to the effort that you put into the business.
                  Though network marketing can be a great career path through
                  which you can earn residual income, i.e., your efforts of
                  today will help you in the future.
                </div>
                <div className="question">
                  Is network marketing the right choice for someone with no
                  marketing background?
                </div>
                <div className="answer">
                  One of the best things about network marketing is that is
                  doesn’t rely on your area of education. No matter what
                  qualifications you have or what background you have, you can
                  achieve success in network marketing. All you require is a
                  strong desire to be successful in network marketing.
                </div>
                <div className="question">
                  Why aren’t more people involved in network marketing?
                </div>
                <div className="answer">
                  Network marketing is like just another profession. For a fact,
                  we know that the profession of an engineer is a great
                  profession, but that doesn’t mean everybody will only become
                  an engineer. Likewise, not everyone will join network
                  marketing. Some visionary people will become a part of it and
                  some might just criticize it.
                </div>
                <div className="question">
                  I am too busy and I don’t have time to start this business.
                  What should I do?
                </div>
                <div className="answer">
                  If you don&#39;t have much time and are still looking for a
                  side-project that can help you become financially independent,
                  then there is nothing better than network marketing. Though it
                  may seem like too much of a work, one lovable fact about
                  network marketing is that you can divide your work and take it
                  slow.
                </div>
                <div className="question">
                  Is this business for those who can’t do anything else?
                </div>
                <div className="answer">
                  No, network marketing is a profession where a person from any
                  background can come and make a living. Every person has his
                  own reason to start with network marketing, be it financial or
                  productive. To some people, network marketing is a profession
                  for the money, to others, it is a profession through which
                  they can spare more time for other activities. In short,
                  network marketing is for everyone.
                </div>
                <div className="question">
                  Is network marketing a legitimate business?
                </div>
                <div className="answer">
                  Yes, it is. As a network marketer, you are the link between
                  the manufacturer and the customer. On the other hand, there
                  are some organizations out there that might not be genuine.
                </div>
                <div className="question">
                  What is the start-up cost for network marketing?
                </div>
                <div className="answer">
                  Most of the genuine and legitimate organizations don’t charge
                  money. However, there might be some training module provided
                  by the company which is to be paid for. Just make sure that
                  you have researched the company before deciding to join it. If
                  most of the company’s income is from its training modules,
                  then the chances are that the company is illegitimate or a
                  get-rich-quick scheme.
                </div>
                <div className="question">What is Loopin?</div>
                <div className="answer">
                  Loopin is a customer relationship management tool that
                  integrates with your Facebook messenger. It has a list of
                  features that could help you multitask and make the right
                  decisions while networking. One fundamental feature of Loopin
                  is that it can act as a progress tracking tool, which
                  live-tracks the progress of your prospects and shows you the
                  most active ones on top. Thus, informing you of the best
                  prospect to contact and meet.
                </div>
                <div className="question">
                  How will Loopin help me meet my Network Marketing goals?
                </div>
                <div className="answer">
                  As the word is steadily going more and more digital, Loopin
                  would prove to be a great companion for your network marketing
                  journey. This digital tool is your one-time spot for getting
                  in touch with your prospects in the form of messages or voice
                  notes. All you need is a Facebook messenger app. It is as
                  simple as that.
                </div>
                <div className="question">
                  Do I need to download an app to access Loopin?
                </div>
                <div className="answer">
                  No. The only application that you require is the Facebook
                  messenger.
                </div>
                <div className="question">How do I Use Loopin?</div>
                <div className="answer">
                  Loopin is an easy-to-use tool that simply integrates with your
                  Facebook messenger application. Through it, you can share a
                  link with your prospects online and offline through a QR code.
                  It has a simple dashboard through which you can access your
                  recent activities and your prospect list. In short, it is a
                  simple prospect manager in your pocket.
                </div>
                <div className="question">What is Loopin superbot?</div>
                <div className="answer">
                  Loopin’s superbot is your companion that’ll help start your
                  initial conversation with the prospects. It will try to get
                  some information from the prospect’s side, such as whether
                  they are willing to take the opportunity, how much time they
                  are ready to devote to network marketing, and finally, it will
                  help them set up a meeting with you.
                </div>
                <div className="question">
                  Can I use Loopin on my mobile phone?
                </div>
                <div className="answer">
                  Yes, Loopin is a light smartphone-friendly tool.
                </div>
                <div className="question">
                  How can I see my prospect’s progress?
                </div>
                <div className="answer">
                  At the home screen, you can find an option ‘My Prospects/ My
                  List,’ upon clicking it, you can access the information
                  regarding your prospects with whom you have shared your Loopin
                  link. Inside, you’ll find three more options: &#39;recent 20’,
                  ‘most active,’ and &#39;prospect search.’ The tool will show
                  you information about every prospect. Moreover, the tool also
                  allows you to search for the progress of any specific
                  prospect.
                </div>
                <div className="question">
                  WHAT IS AN INVITE URL? HOW CAN I SHARE IT WITH MY LIST?
                </div>
                <div className="answer">
                  Invite URL is a unique link that you can share with your
                  potential prospects via email, SMS, social media, or other
                  messaging apps, which your potential prospects can click to
                  start their network marketing journey. Once the potential
                  prospect clicks on the Invite URL shared by you, the
                  prospecting journey will begin.  Sharing an Invite URL is
                  extremely easy and simple; all you need to do is just click on
                  INVITE &amp; SHARE on the main menu and then select ONLINE.
                  Once you click on the online option, you will get the INVITE
                  URL.
                </div>
                <div className="question">
                  WHAT IS A MESSAGE CARD? HOW CAN I SHARE IT WITH MY LIST?
                </div>
                <div className="answer">
                  A message card is another way of making online connections.
                  Message cards offer a way for you to hold richer
                  in-conversation experience than standard text messages as
                  message cards let you integrate buttons, images, and much
                  more. However, a message card can only be shared with people
                  who are on your Facebook friend list.  Sharing a Message Card
                  is easy. First, click on INVITE &amp; SHARE on the main menu
                  and then select ONLINE, after this select MESSAGE CARD.
                </div>
                <div className="question">
                  HOW AND WHERE CAN I USE MY QR CODE?
                </div>
                <div className="answer">
                  QR code is an offline method of sharing your opportunity with
                  your potential prospects. You can find your personalized QR
                  Code under the Invite &amp; Share option. This comes handy
                  during live events and seminars; people can just scan your QR
                  code to know more about you and your successful business.
                </div>
                <div className="question">
                  WHAT WILL HAPPEN IF MY PROSPECT CLICKS ON THE INVITE URL THAT
                  I SHARED?
                </div>
                <div className="answer">
                  When a prospect clicks on the Invite URL that was shared by
                  you, he will be redirected to our SUPERBOT (a Facebook
                  messenger window), where he will be briefed about the
                  opportunity that you have to offer. The SUPERBOT will also
                  gather certain important information like what are his future
                  goals and how many hours the prospect is willing to devote?
                  All this information will help you in identifying &amp;
                  qualifying the right prospects for your network.
                </div>
                <div className="question">WHAT IS MY PROSPECT/MY LIST?</div>
                <div className="answer">
                  My Prospect or My List is a directory of potential clients. It
                  is one of the most valuable assets of your business. My
                  Prospect / My List option is available on the main menu of the
                  SUPERBOT; it will show you a list of people who’ve
                  successfully passed the first phase of the recruitment process
                  by showing interest in what you have to offer.
                </div>
                <div className="question">WHAT IS MY DASHBOARD?</div>
                <div className="answer">
                  A dashboard is a type of graphical user interface that gives
                  at-a-glance views of key performance indicators (KPIs)
                  relevant to your network. From your dashboard, you can look at
                  your prospect’s activity, search for a specific prospect, and
                  you can also keep track of their progress.
                </div>
                <div className="question">
                  WHAT IS PROSPECT SEARCH IN MY PROSPECT / MY LIST OPTION?
                </div>
                <div className="answer">
                  Prospect search allows you to look for a specific prospect by
                  typing his / her name. This will further help you to manage
                  your contacts and list as you don’t have to go through your
                  contact history again and again to find details of a specific
                  prospect; simply type their names and their details will
                  appear.
                </div>
                <div className="question">
                  WHAT IS PROSPECT SCORING? HOW IS IT CALCULATED?
                </div>
                <div className="answer">
                  When a visitor converts to a prospect, the prospect is given a
                  score. This score changes as your prospect interact with your
                  marketing assets. This scoring will provide you with an idea
                  to measure the prospect&#39;s intent and interest.
                </div>
                <div className="question">WHAT ARE RECENT 20?</div>
                <div className="answer">
                  As the name suggests, the Recent 20 is a list of your newly
                  acquired 20 prospects.
                </div>
                <div className="question">WHO ARE MY HOT PROSPECTS?</div>
                <div className="answer">
                  The most active members in your list are listed as your ‘Hot
                  Prospects.’ By knowing them, you’ll certainly figure out the
                  best prospects that you can recruit.
                </div>
                <div className="question">
                  WHAT DOES RECENT ACTIVITY INCLUDE?
                </div>
                <div className="answer">
                  LOOPIN provides you with a feature that lets you track the
                  progress of your prospects. Thus, allowing you to keep a tab
                  on your network’s growth in a flexible way. In Recent
                  Activity, you can quickly take a look at what your prospects
                  are up to, and the list is continuously updated so that you
                  don’t miss onto anything important.
                </div>
                <div className="question">
                  WHAT IS LOOPIN’S CONTENT LIBRARY?
                </div>
                <div className="answer">
                  LOOPIN not only provides you a platform to get in touch with
                  your prospects but also a platform that you can use to store
                  industry-related links, articles, news stories, and much more.
                  The content library is a storehouse of all the useful
                  resources that can help your prospect navigate through the
                  obstacles of network marketing.
                  <br />
                  <br />
                  You can save third-party links, websites, videos, quotations,
                  articles, news, success stories, reading material, tutorial
                  videos, etc. These can be further shared with your prospect in
                  order to pursue him or motivate him.
                </div>
                <div className="question">
                  WHAT ALL IS THERE IN LOOPIN’S CONTENT LIBRARY?
                </div>
                <div className="answer">
                  Loopin’s content library contains all the essential learning
                  material that’ll be beneficial for your prospects, especially
                  if they are new to the field. Our content library has all the
                  reading material and tutorial videos that can help your
                  prospects stay motivated and build a successful network
                  marketing business.
                </div>
                <div className="question">
                  HOW CAN I SHARE THINGS FROM MY CONTENT LIBRARY?
                </div>
                <div className="answer">
                  Sharing anything from your content library is quite easy; all
                  you have to do is just select the content library on the main
                  menu and once the library opens, just choose the file that you
                  want to share with your prospect list. Once you’ve made a
                  selection, the list will automatically appear, just select the
                  one with whom you want to share the file with.
                </div>
                <div className="question">
                  WHAT DOES PROSPECT ACTIVITY include?
                </div>
                <div className="answer">
                  The prospect activity tab includes information that might
                  prove to be crucial for your networking process. It reveals
                  the number of times a prospect has visited your website and
                  the percentage of tutorial videos watched by your prospect. In
                  addition to that, it also gives you the date and time of your
                  prospect&#39;s last activity.
                </div>
                <div className="question">
                  HOW CAN I ADD NOTES FOR MY PROSPECTS?
                </div>
                <div className="answer">
                  Adding notes for your prospect is an easy task. Start with
                  searching for a prospect about whom you want to add a note.
                  The next step is to click on the NOTES tab and then click on
                  the Add Note. Notes will help you organize and capture the
                  details of your prospects.
                </div>
                <div className="question">
                  WHERE CAN I SEE ALL THE VOICE MESSAGES THAT WERE EXCHANGED
                  BETWEEN ME &amp; MY PROSPECTS?
                </div>
                <div className="answer">
                  All the voice notes are available at the chat window under the
                  voice tab. Each of your prospects will have an individual chat
                  window; hence you can take a look at the voice notes that you
                  have sent to them individually.
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Faq;
