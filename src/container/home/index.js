/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-no-target-blank */
import React, { Component } from "react";
import Swiper from "react-id-swiper";
import "swiper/css/swiper.css";
import { Link } from "react-router-dom";
import { Container, Row, Col, Hidden, Visible } from "react-grid-system";
// import { Navigation, Pagination } from "swiper/js/swiper.esm";

import Video from "../../assets/img/SampleVideo.mp4";
// import Image from "../../assets/img/testimonioal.jpg";

import Icon1 from "../../assets/img/qualify.svg";
import Icon2 from "../../assets/img/manage.svg";
import Icon3 from "../../assets/img/share.svg";
import Icon4 from "../../assets/img/build.svg";
import Book from "../../assets/img/book.png";
// import Homepage from "../../assets/img/homepage_banner.jpg";
import Play from "../../assets/img/play.png";
import Popup from "../../component/modal";

// import scrollToComponent from "react-scroll-to-component";
// import { ReactComponent as Share } from "../../assets/img/Share.svg";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      swiper: null,
      disableNextButton: false,
      disablePrevButton: true,
      showPopup: false,
      fields: {
        username: "",
        emailid: "",
        message: ""
      },
      errors: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
    props.setShowSection(true);
  }
  OpenPopup = () => {
    this.setState({
      showPopup: true
    });
    document.body.style.position = "fixed";
    document.body.style.overflow = "Hidden";
  };
  closePopup = () => {
    this.setState({
      showPopup: false
    });
    document.body.style.position = "unset";
    document.body.style.overflow = "unset";
  };
  goNext = () => {
    if (this.state.swiper !== null) {
      this.state.swiper.slideNext();
    }
  };

  goPrev = () => {
    if (this.state.swiper !== null) {
      this.state.swiper.slidePrev();
    }
  };

  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm(e) {
    e.preventDefault();
    this.validateForm(() => {
      if (
        Object.keys(this.state.errors).filter(
          x => this.state.errors[x] !== undefined
        ).length === 0
      ) {
        const { emailid, message, username } = this.state.fields;
        fetch(
          `https://us-central1-loopin-fad04.cloudfunctions.net/sendMail?from=${emailid}&to=kanit@guesthouser.com&subject=${username}&message=${message}`,
          {
            method: "POST"
          }
        );
      }
    });
  }

  validateUserName = cb => {
    const {
      fields: { username }
    } = this.state;

    if (username.length !== 0) {
      if (!username.match(/^[a-zA-Z ]*$/)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: "*Please enter alphabet characters only."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              username: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            username: "*Please enter your full name"
          }
        },
        cb
      );
    }
  };

  validateEmailID = cb => {
    const {
      fields: { emailid }
    } = this.state;

    if (emailid.length !== 0) {
      const pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(emailid)) {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: "*Please enter valid email-ID."
            }
          },
          cb
        );
      } else {
        this.setState(
          {
            errors: {
              ...this.state.errors,
              emailid: undefined
            }
          },
          cb
        );
      }
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            emailid: "*Please enter your email-ID"
          }
        },
        cb
      );
    }
  };

  validateMessage = cb => {
    const {
      fields: { message }
    } = this.state;
    if (message.length === 0) {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            message: "*Please enter your message."
          }
        },
        cb
      );
    } else {
      this.setState(
        {
          errors: {
            ...this.state.errors,
            message: undefined
          }
        },
        cb
      );
    }
  };
  validateForm = cb => {
    this.validateMessage(() =>
      this.validateEmailID(() => this.validateUserName(cb))
    );
  };
  render() {
    const param = {
      // modules: [Navigation, Pagination, Autoplay],
      slidesPerView: 1,
      spaceBetween: 0,
      loop: true,
      autoplay: {
        delay: 10000,
        disableOnInteraction: false
      },
      pagination: {
        el: ".swiper-pagination.customized-swiper-pagination",
        type: "bullets",
        clickable: true
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },
      breakpoints: {
        769: {
          slidesPerView: 1
        }
      }
    };
    // const params = {
    //   // modules: [Navigation, Pagination],
    //   // pagination: {
    //   //   el: ".swiper-pagination",
    //   //   type: "bullets",
    //   //   clickable: true
    //   // },
    //   // navigation: {
    //   //   nextEl: ".swiper-button-next",
    //   //   prevEl: ".swiper-button-prev"
    //   // },
    //   // on: {
    //   //   slideChange: () => {
    //   //     // console.log("index", this.state.swiper.activeIndex);
    //   //     if (3 - 1 === this.state.swiper.activeIndex) {
    //   //       this.setState({
    //   //         disableNextButton: true
    //   //       });
    //   //     } else if (this.state.swiper.activeIndex === 0) {
    //   //       this.setState({
    //   //         disablePrevButton: true
    //   //       });
    //   //     } else {
    //   //       this.setState({
    //   //         disablePrevButton: false,
    //   //         disableNextButton: false
    //   //       });
    //   //     }
    //   //   }
    //   // },
    //   spaceBetween: 30
    // };
    return (
      <div className="wrapper">
        <Hidden xs sm>
          <div className="slider-wrapper">
            {/* <div className="slider-overlay" /> */}
            <Container fluid style={{ maxWidth: "1200px" }}>
              <div className="slider">
                <div className="custom-slide">
                  <Row>
                    <Col sm={12}>
                      <div className="slider-content">
                        <div className="heading">
                          The Next Generation of <br />
                          Network Marketing in Just 4 Minutes
                        </div>
                        {/* <div className="big-title">NETWORK MARKETING</div> */}
                        {/* <div className="title">IN JUST 4 MINUTES</div> */}
                        <div className="subheading">
                          Grow your network by understanding people who are
                          interested
                          <br />
                          and who are not.
                        </div>
                        <div
                          class="HomePage__Banner__video mt-48"
                          onClick={this.OpenPopup}
                        >
                          <div class="HomePage__Banner__video__control">
                            <div class="HomePage__Banner__video__control__2">
                              <img src={Play} alt="Play" />
                            </div>
                          </div>{" "}
                          <div class="HomePage__Banner__video__title ml-12">
                            <div class="HomePage__Banner__video__title__text">
                              Watch
                            </div>{" "}
                            <div class="HomePage__Banner__video__title__subtext">
                              INTRO VIDEO
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* <div>
                      <img src={Homepage} />
                    </div> */}
                    </Col>
                  </Row>
                </div>
                {/* <Swiper
                {...params}
                getSwiper={swiper => this.setState({ swiper })}
              >
                <div className="custom-slide">
                  <Row>
                    <Col sm={6}>
                      <div className="slider-content">
                        <div className="title">THE NEXT GENERATION OF</div>
                        <div className="big-title">NETWORK MARKETING</div>
                        <div className="title">IN JUST 4 MINUTES</div>
                      </div>
                    </Col>
                    <Col sm={6}>
                      <video
                        poster={PosterImage1}
                        src={Video}
                        id="video-bg"
                        // autoPlay
                        // loop
                        // muted
                        // webkit-playsinline="true"
                        // playsinline="true"
                        controls
                      />
                    </Col>
                  </Row>
                </div>
              </Swiper> */}
                {/* <a
                href="Javascript:void(0)"
                onClick={this.goPrev}
                className={`swiper-prev-btn ${this.state.disablePrevButton &&
                  "disabled"}`}
              />
              <a
                href="Javascript:void(0)"
                onClick={this.goNext}
                className={`swiper-next-btn ${this.state.disableNextButton &&
                  "disabled"}`}
              /> */}
              </div>
            </Container>
          </div>
        </Hidden>
        <Visible sm xs>
          <div className="mobile-view">
            <div className="heading">
              The Next Generation of Network Marketing in Just 4 Minutes
            </div>
            <div className="subheading">
              Grow your network by understanding people who are interested and
              who are not.
            </div>
            <div className="video-view">
              {/* <video src={Video} id="video-bg" controls /> */}
              <div className="button d-inline-block m-top" onClick={this.OpenPopup}>
                Watch Intro
              </div>
            </div>
          </div>
        </Visible>
        <div className="first-box-section">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={12}>
                <div className="box">
                  <div className="heading">
                    A SOLUTION FOR EVERY NETWORK MARKETER
                  </div>
                  <div className="content">
                    An automated network marketing tool with built-in
                    intelligence for managing, contacting, <br /> and growing
                    you network in the digital world.
                  </div>
                  <Link to="/whatloopin" className="button d-inline-block">Know More</Link>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <div className="second-section">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={12}>
                <div className="heading">WHAT CAN YOU DO WITH LOOPIN?</div>
                <div className="subheading">
                  We can actually help you grow your network
                </div>
              </Col>
              <Col sm={3}>
                <div className="box">
                  <div className="icon">
                    <img src={Icon1} alt="" />
                  </div>
                  <div className="heading">Qualify Prospects</div>
                  <div className="content">
                    Talk only to those who are interested in what you have to
                    offer.
                  </div>
                </div>
              </Col>
              <Col sm={3}>
                <div className="box">
                  <div className="icon">
                    <img src={Icon2} alt="" />
                  </div>
                  <div className="heading">MANAGE PROSPECTS</div>
                  <div className="content">
                    Know what your prospects are exactly up to.
                  </div>
                </div>
              </Col>
              <Col sm={3}>
                <div className="box">
                  <div className="icon">
                    <img src={Icon3} alt="" />
                  </div>
                  <div className="heading">Share Content</div>
                  <div className="content">
                    Send the right message to your prospects at the right place.
                  </div>
                </div>
              </Col>
              <Col sm={3}>
                <div className="box">
                  <div className="icon">
                    <img src={Icon4} alt="" />
                  </div>
                  <div className="heading">BUILD NETWORK</div>
                  <div className="content">
                    Do the right things with the right people and at the right
                    place.
                  </div>
                </div>
              </Col>
            </Row>
            <Row justify="center">
              <Col ms={12} className="text-center">
                <Link to="/whyloopin" className="button d-inline-block">Know More</Link>
              </Col>
            </Row>
          </Container>
        </div>
        <div className="third-section">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={7}>
                <div className="book">
                  <div className="heading">
                    Experience the new avatar of Network Marketing
                  </div>
                  <div className="subheading">
                    Learn how digital can transform your network marketing
                    business in this e-Book by Kamal Aanjna.
                  </div>
                  <div className="text-left sm-down-text-center">
                    <Link to="/network-marketing-5.0-ebook" className="button d-inline-block m-top">Read More</Link>
                  </div>
                </div>
              </Col>
              <Col sm={5}>
                <div className="book-image">
                  <img src={Book} alt="book" />
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        {/* <div className="left-section first">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={6}>
                <div className="box">
                  <div className="title">What is LOOPIN?</div>
                  <div className="subtitle">
                    A simplified, streamlined, and focused network marketing
                    tool to deliver innovative sales enabled solutions. Build
                    especially for network marketing leaders like you, LOOPIN is
                    changing the way network marketers manage contacts, connect
                    with prospects, creates content, and build their business.
                    It’s the best CRM for all your network marketing companies.
                  </div>
                  <div className="button">
                    <Link to="/whatloopin">Read More</Link>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div> */}
        {/* <div className="right-section first">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row justify="end">
              <Col sm={6}>
                <div className="box">
                  <div className="title">Why LOOPIN?</div>
                  <div className="subtitle">
                    Built only for the next-gen network marketer, LOOPIN works
                    on your phone, tablet or computer. All data synced between
                    the web and the app, so you can travel worry-free wherever
                    your business takes you.
                    <br />
                    <br />
                    We understand the growing needs of the network marketing
                    industry. Therefore, we’ve built a tool that tracks,
                    manages, sends messages, creates… schedules and shares
                    everything using just one single dashboard - freeing up more
                    time for you to focus more on expanding your network.
                  </div>
                  <div className="button">
                    <Link to="/whyloopin">Read More</Link>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div> */}
        {/* <div className="client-container">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row justify="center">
              <Col sm={5}>
                <div className="client-image">
                  <img src={Image} />
                </div>
              </Col>
              <Col sm={7}>
                <div className="client-Details">
                  <div className="heading">YOU ARE AT THE RIGHT PLACE!</div>
                  <div className="subheading">
                    <span className="line" />
                    Meet other people who feel the same
                  </div>
                  <div className="content">
                    LOOPIN has provided me an easy and fast way to manage
                    existing people on the list and also a way to reach out to
                    new people. Since the time I signed up for it, the tool has
                    acted like my 24*7 personal assistant – from giving out
                    suggestions, to tracking, managing and finding ways to
                    expand my brand’s presence and my personal reach.{" "}
                  </div>
                  <div className="client-name">Ramit Kumar</div>
                </div>
              </Col>
            </Row>
          </Container>
        </div> */}
        {/******************* testimonials ********************/}
        <div className="info testimonial" ref={this.testimonialRef}>
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col xs={12} sm={12}>
                <div className="info-container">
                  <div className="heading">YOU ARE AT THE RIGHT PLACE!</div>
                  <div className="subheading">
                    Meet other people who feel the same
                  </div>
                </div>
              </Col>
            </Row>
            <div className="testmonial-outer-wrap">
              <Swiper {...param}>
                <div className="testimonials custom-test">
                  <div className="text">
                    LOOPIN has provided me an easy and fast way to manage
                    existing people on the list and also a way to reach out to
                    new people. Since the time I signed up for it, the tool has
                    acted like my 24*7 personal assistant – from giving out
                    suggestions, to tracking, managing and finding ways to
                    expand my brand’s presence and my personal reach. <br />
                  </div>
                  <div className="name">
                    <div class="title">Rohit Singh</div>
                    <div className="subtitle">Delhi</div>
                  </div>
                </div>
                <div className="testimonials custom-test">
                  <div className="text">
                    LOOPIN is my personal one-time stop where I can easily track
                    the activities of my prospects as well their growth. It has
                    definitely made my professional life simpler and flexible
                    with its user-friendly interface. The best part is, now I
                    can travel worrisome because everything I need to know about
                    my prospects is just available on my smartphone.
                  </div>
                  <div className="name">
                    <div className="title">Surbhi Srivastava</div>
                    <div className="subtitle">Assam</div>
                  </div>
                </div>
              </Swiper>
            </div>
          </Container>
        </div>
        <div className="third-section" style={{background: "#04a0ff"}}>
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={7}>
                <div className="book">
                  <div className="heading color-white">
                    NETWORK YOUR DREAMS & MARKET YOUR BUSINESS IN THE DIGITAL
                    WORLD
                  </div>
                  <div className="subheading color-white">
                    Get everything you need to know about digital network
                    marketing in the Success Blueprint Course by
                    Kamal Aanjna.
                  </div>
                  <div className="text-left sm-down-text-center">
                    <Link to="/signin" className="button d-inline-block m-top">Know More</Link>
                  </div>
                </div>
              </Col>
              <Col sm={5}>
                <div className="info-video">
                  <iframe className="full-w" width="560" height="315" src="https://www.youtube.com/embed/vFxN9DSZKBo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  {/* <video
                    // poster={PosterImage}
                    width="400"
                    height="300"
                    src="https://firebasestorage.googleapis.com/v0/b/loopin-fad04.appspot.com/o/PROMO%20Loopin%20Tool_English%20Without%20Thank%20You.mp4?alt=media&token=8c9e2dd3-03fd-4bf0-a9a9-32d21769ca76"
                    // autoPlay
                    // loop
                    // muted
                    // webkit-playsinline="true"
                    // playsinline="true"
                    controls
                    className="bg-black"
                  /> */}
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        {/* <div className="massage-container home">
          <Container fluid style={{ maxWidth: "1200px" }}>
            <Row>
              <Col sm={6}>
                <div className="heading">
                  STILL NEED TO TALK TO US PERSONALLY?
                </div>
                <div className="subheading">We have a plan.</div>
                <div className="subtitle">
                  Drop in your details and our team will get in touch with you.
                </div>
              </Col>
              <Col sm={6}>
                <form
                  className="form_wrapeer "
                  method="post"
                  name="userRegistrationForm"
                  onSubmit={this.submituserRegistrationForm}
                >
                  <div className="input-wrap">
                    <input
                      type="text"
                      name="username"
                      placeholder="Name"
                      autoComplete="false"
                      value={this.state.fields.username}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.username}
                    </span>
                  </div>
                  <div className="input-wrap">
                    <input
                      type="text"
                      autoComplete="false"
                      placeholder="Email"
                      name="emailid"
                      value={this.state.fields.emailid}
                      onChange={this.handleChange}
                    />
                    <span className="errorMsg">
                      {this.state.errors.emailid}
                    </span>
                  </div>
                  <div className="input-wrap">
                    <textarea
                      name="message"
                      onChange={this.handleChange}
                      placeholder="Message"
                      value={this.state.fields.message}
                    />
                    <span className="errorMsg error">
                      {this.state.errors.message}
                    </span>
                  </div>

                  <div className="input-wrap">
                    <button type="submit" className="button">
                      Submit
                    </button>
                  </div>
                </form>
              </Col>
            </Row>
          </Container>
        </div> */}
        {this.state.showPopup ? (
          <Popup
            text='Click "Close Button" to hide popup'
            closePopup={this.closePopup}
            videoType={'youtube'}
            videoId={'eHVP5QDZsGM'}
          // url="https://firebasestorage.googleapis.com/v0/b/loopin-fad04.appspot.com/o/PROMO%20Loopin%20Tool_English%20Without%20Thank%20You.mp4?alt=media&token=8c9e2dd3-03fd-4bf0-a9a9-32d21769ca76"
          />
        ) : null}
      </div>
    );
  }
}

export default Home;
