export function getIndexBy(obj, arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i][obj.field] === obj.value) {
      return i;
    }
  }
  return -1;
}

export function getRandomString(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}