import React, { Fragment } from "react";
import "../assets/sass/main.scss";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ToastContainer } from 'react-toastify';
import ComingSoon from "../container/comingSoon";
import Home from "../container/home";
import About from "../container/about";

import Signup from "../container/loginSignup/signup";
import Signin from "../container/loginSignup/signin";
import WhatLoopin from "../container/whatLoopin";
import Course from "../container/traningCourse";
import Ebook from "../container/ebook";
import Pricing from "../container/price";
import ProPricing from "../container/price/pro";

import WhyLoopin from "../container/whyLoopin";
import Header from "../component/header";
import Footer from "../component/footer";
import Errorpage from "../container/404";
import PdfViwer from "../container/pdf";
import Blog from "../container/blog";
import BlogContent from "../container/blog/blogDetails";
import Cart from "../container/cart";
import ForgotPassword from "../container/loginSignup/forgotPassword";
import Faq from "../container/faq";
import PrivacyPolicy from "../container/privacyPolicy";
import TermsAndConditions from "../container/termsAndConditions";
import ThankYou from "../container/thankYou";
import EbookThankYou from "../container/thankYou/ebook";
import TrainingCourseThankYou from "../container/thankYou/trainingCourse";

import 'react-toastify/dist/ReactToastify.css';
const RouteWithProps = ({
  path,
  exact,
  strict,
  component: Component,
  location,
  ...rest
}) => (
    <Route
      path={path}
      exact={exact}
      strict={strict}
      location={location}
      render={props => <Component {...props} {...rest} />}
    />
  );

const getUser = () =>
  JSON.parse((x => (x ? x : null))(localStorage.getItem("user")));

function App() {
  const [showSection, setShowSection] = React.useState(true);
  const [cartValue, setCartValue] = React.useState(
    JSON.parse(localStorage.getItem("cart")) || []
  );
  const [loggedIn, setLoggedIn] = React.useState(getUser());

  React.useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cartValue));
  }, [cartValue, setCartValue]);

  React.useEffect(() => {
    if (!loggedIn) {
      setCartValue([]);
    }
    localStorage.setItem("user", JSON.stringify(loggedIn));
  }, [loggedIn, setLoggedIn, setCartValue]);

  return (
    <Router>
      <div className="App">
        {showSection && (
          <Header
            loggedIn={loggedIn}
            setLoggedIn={setLoggedIn}
            cartValue={cartValue}
          />
        )}
        <Switch>
          {/* {process.env.REACT_APP_ENV === 'production' &&
            <Route
              exact
              path="/"
              render={props => (
                <ComingSoon {...props} />
              )}
            />
          } */}

          <Route
            exact
            path="/home"
            render={props => (
              <Home setShowSection={setShowSection} {...props} />
            )}
          />
          <Route
            exact
            path="/signup"
            render={props => (
              <Signup
                setLoggedIn={setLoggedIn}
                setShowSection={setShowSection}
                {...props}
              />
            )}
          />
          <Route
            exact
            path="/signin"
            render={props => (
              <Signin
                setLoggedIn={setLoggedIn}
                setShowSection={setShowSection}
                {...props}
              />
            )}
          />
          <Route
            exact
            path="/whatloopin"
            render={props => (
              <WhatLoopin setShowSection={setShowSection} {...props} />
            )}
          />
          <Route
            exact
            path="/whyloopin"
            render={props => (
              <WhyLoopin setShowSection={setShowSection} {...props} />
            )}
          />
          <Route
            exact
            path="/network-marketing-5.0-ebook"
            render={props => (
              <Ebook setShowSection={setShowSection} {...props} />
            )}
          />
          <Route
            exact
            path="/network-marketing-5.0-ebook/thank-you"
            render={props => <EbookThankYou setShowSection={setShowSection} {...props} />}
          />
          <Route
            exact
            path="/network-marketing-course"
            render={props => (
              <Course
                loggedIn={loggedIn}
                setLoggedIn={setLoggedIn}
                setShowSection={setShowSection} 
                {...props}
              />
            )}
          />
          <Route
            exact
            path="/network-marketing-course/thank-you"
            render={props => <TrainingCourseThankYou setShowSection={setShowSection} {...props} />}
          />
          <Route
            exact
            path="/price-plan"
            render={props => (
              <Pricing
                loggedIn={loggedIn}
                cartValue={cartValue}
                setCartValue={setCartValue}
                setShowSection={setShowSection}
                {...props}
              />
            )}
          />
          <Route
            exact
            path="/price-plan/pro-enterprise"
            render={props => (
              <ProPricing
                loggedIn={loggedIn}
                setShowSection={setShowSection}
                {...props}
              />
            )}
          />
          <Route
            exact
            path="/thank-you"
            render={props => (
              <ThankYou
                loggedIn={loggedIn}
                cartValue={cartValue}
                setCartValue={setCartValue}
                setShowSection={setShowSection}
                {...props}
              />
            )}
          />
          <Route
            exact
            path="/blog"
            render={props => (
              <Blog setShowSection={setShowSection} {...props} />
            )}
          />
          <Route
            exact
            path="/blog/:heaing/:id"
            render={props => (
              <BlogContent setShowSection={setShowSection} {...props} />
            )}
          />
          <Route
            exact
            path="/cart"
            render={props => (
              <Cart
                cartValue={cartValue}
                setCartValue={setCartValue}
                loggedIn={loggedIn}
                setShowSection={setShowSection}
                {...props}
              />
            )}
          />
          <Route
            exact
            path="/about-us"
            render={props => (
              <About setShowSection={setShowSection} {...props} />
            )}
          />
          <Route
            exact
            path="/pdfview"
            render={props => (
              <PdfViwer setShowSection={setShowSection} {...props} />
            )}
          />
          <Route
            exact
            path="/resetpassword"
            render={props => (
              <ForgotPassword setShowSection={setShowSection} {...props} />
            )}
          />
          <Route
            exact
            path="/faq"
            render={props => <Faq setShowSection={setShowSection} {...props} />}
          />
          <Route
            exact
            path="/privacy-policy"
            render={props => <PrivacyPolicy setShowSection={setShowSection} {...props} />}
          />
          <Route
            exact
            path="/terms-and-conditions"
            render={props => <TermsAndConditions setShowSection={setShowSection} {...props} />}
          />
          <RouteWithProps
            setShowSection={setShowSection}
            component={Errorpage}
          />
        </Switch>
        <ToastContainer autoClose={6000} position="top-right" hideProgressBar pauseOnHover draggable />
        {showSection && <Footer />}
      </div>
    </Router>
  );
}

export default App;
