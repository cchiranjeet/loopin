// import logo from "../../assets/img/blackLogo.svg";
import { toast } from 'react-toastify';
import { getRandomString } from "../../utils/common";
import * as firebase from "firebase";
import "firebase/auth";
import "firebase/firestore";
import { createThinkificUser, getThinkificUserByEmail } from "../../api/thinkific/user";
import { createThinkificEnrollment, getThinkificEnrollment } from "../../api/thinkific/enrollment";
import { getThinkificProduct } from "../../api/thinkific/product";
import { getBot, getBotSubscription, purchaseBotSubscription } from "../../api/firebase/common";
import { useRazorpay } from "../razorpay/checkout";

export function usePurchase({ setLoading }) {
  const razorpay = useRazorpay();

  async function purchaseBot(data) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user } = data;
        const { email, username, plan, mobileno } = data;
        let thinkificUser;
        const confirmEmailParams = {
          to: email,
          name: username,
        };

        // CHECK IF USER IS ALREADY ENROLLED TO THIS COURSE
        const botSubscription = await getBotSubscription(email, plan);
        if (botSubscription) {
          toast.info(`You have already purchased the ${plan} bot`);
          return reject();
        }

        // GET BOT PRICE
        const bot = await getBot(data.plan);
        setLoading(false);
        razorpay.openCheckout({
          name: username,
          email: email,
          description: bot.name,
          amount: bot.price
        }, async (razorpayResponse) => {
          setLoading(true);
          await purchaseBotSubscription({
            username: username,
            email: email,
            mobileno: mobileno,
            razorpay_payment_id: razorpayResponse.razorpay_payment_id,
            plan: plan,
            amount: bot.price,
          });

          return resolve(true);
        });
      } catch (err) {
        console.log(err);
        toast.error("Some error occurred, please try again later.");
        return reject(err);
      }
    });
  }

  async function purchaseCourse(data) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user } = data;
        const { email, username, courseName, thinkificCourseId, thinkificProductId } = data;
        const password = getRandomString(6);
        let thinkificUser;
        const confirmEmailParams = {
          to: email,
          name: username,
        };
        // user is not logged in
        if (!user) {
          user = await fetch(`${process.env.REACT_APP_FIREBASE_API_ENDPOINT}/getUserByEmail?email=${email}`, {
            method: "GET",
            headers: {
              "Content-Type": "application/json"
            }
          });
          user = await user.json();

          if (!user) {
            await new Promise(resolve => {
              firebase
                .auth()
                .createUserWithEmailAndPassword(
                  email,
                  password
                )
                .then(user => {
                  confirmEmailParams['loopin_credentials'] = {
                    email: email,
                    password: password,
                  };
                  return resolve(user);
                })
                .catch(error => {
                  return resolve(false);
                });
            });

            user = await new Promise(resolve => {
              firebase.auth().onAuthStateChanged(user => {
                if (user) {
                  user
                    .updateProfile({ displayName: username })
                    .then(() => {
                      return resolve(user);
                    });
                }
              });
            });
          }
        }

        // create user in thinkific
        thinkificUser = await getThinkificUserByEmail(email);
        if (!thinkificUser) {
          thinkificUser = await createThinkificUser({
            first_name: username.split(" ")[0],
            last_name: username.split(" ")[1],
            email: email,
            password: password
          });
          confirmEmailParams['thinkific_credentials'] = {
            email: email,
            password: password,
          };
        }

        // CHECK IF USER IS ALREADY ENROLLED TO THIS COURSE
        const enrollment = await getThinkificEnrollment(thinkificUser.id, thinkificCourseId);
        if (enrollment) {
          toast.info(`You have already enrolled for ${courseName}`, {
            position: toast.POSITION.TOP_RIGHT
          });
          return reject();
        }

        // GET THINKIFIC PRODUCT FOR PRICE
        const thinkificProduct = await getThinkificProduct(thinkificProductId);
        setLoading(false);
        // this.setState({ loading: false });
        razorpay.openCheckout({
          name: username,
          email: email,
          description: thinkificProduct.name,
          amount: thinkificProduct.product_prices[0].price
        }, async (razorpayResponse) => {
          setLoading(true);
          // this.setState({ loading: true });
          // CREATE ENROLLMENT IN THINKIFIC
          await createThinkificEnrollment({
            course_id: thinkificCourseId,
            user_id: thinkificUser.id,
            activated_at: new Date()
          });

          // CREATE ENTRY IN PAYMENTS COLLECTION
          await firebase.firestore().collection("payments").add({
            razorpay_payment_id: razorpayResponse.razorpay_payment_id,
            user_id: user.uid,
            amount: thinkificProduct.product_prices[0].price,
            currency: 'INR',
            date: new Date()
          });

          return resolve({ confirmEmailParams });
        });
      } catch (err) {
        toast.error("Some error occurred, please try again later.");
        return reject(err);
      }
    });
  }

  return {
    purchaseCourse,
    purchaseBot
  }
}

// export default Checkout;