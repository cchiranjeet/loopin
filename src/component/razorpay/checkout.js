// import React, { Component } from "react";
import logo from "../../assets/img/blackLogo.svg";

export function useRazorpay() {

  function openCheckout(data, callback) {
    let options = {
      "key": process.env.REACT_APP_RAZORPAY_API_KEY,
      "amount": data.amount * 100, // 2000 paise = INR 20, amount in paisa
      "name": "Loopin",
      "description": data.description,
      "image": logo,
      "handler": function (response) {
        // console.log(response);
        if (callback) callback(response);
      },
      "prefill": {
        "name": data.name,
        "email": data.email
      },
      // "notes": {
      //   "address": "Hello World"
      // },
      "theme": {
        "color": "#04a0ff"
      }
    };

    let rzp = new window.Razorpay(options);
    rzp.open();
  }

  // render () {
  //   return (
  //     <button onClick={this.openCheckout} className="button">{this.props.btnText}</button> 
  //   )
  // }

  return {
    openCheckout
  }
}

// export default Checkout;