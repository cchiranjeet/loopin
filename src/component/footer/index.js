/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import { Container, Col, Row } from "react-grid-system";
import Logo from "../../assets/img/blackLogo.svg";
import Facebook from "../../assets/img/facebook.svg";
import FacebookHover from "../../assets/img/facebook-hover.svg";
import Insta from "../../assets/img/insta.svg";
import InstaHover from "../../assets/img/insta-hover.svg";
import Twitter from "../../assets/img/twitter.svg";
import TwitterHover from "../../assets/img/twitter-hover.svg";
import Linkedin from "../../assets/img/linkedin.svg";
import LinkedinHover from "../../assets/img/linkedin-hover.svg";
import Youtube from "../../assets/img/youtube.svg";
import YoutubeHover from "../../assets/img/youtube-hover.svg";
// import Massagebox from "../../assets/img/massagebox.svg";
import FbMesseneger from "../../assets/img/fb-messenger.png";

class footer extends Component {
  render() {
    return (
      <div className="footer">
        <Container fluid style={{ maxWidth: "1200px" }}>
          <Row>
            <Col sm={8}>
              <div className="footer-link">
                <ul>
                  <li className="title">
                    <span>Our Services</span>
                  </li>
                  <li>
                    <Link to="/whyloopin">Loopin</Link>
                  </li>
                  <li>
                    <Link to="/network-marketing-5.0-ebook">Hand book</Link>
                  </li>
                  <li>
                    <Link to="/network-marketing-course">Success Blueprint Course</Link>
                  </li>
                </ul>
                <ul>
                  <li className="title">
                    <span>COMPANY</span>
                  </li>
                  <li>
                    <Link to="/about-us">About Us</Link>
                  </li>
                  <li>
                    <Link to="/blog">Our Blog</Link>
                  </li>
                  <li>
                    <a href="https://academy.loopinglobal.com/" target="_blank">Looopin Academy</a>
                  </li>
                </ul>
                <ul>
                  <li className="title">
                    <span>SUPPORT</span>
                  </li>
                  <li>{/* <Link to="/">Support Page</Link> */}</li>
                  <li>
                    <Link to="/faq">FAQs</Link>
                  </li>
                  <li>
                    <Link to="/terms-and-conditions">Terms & Conditions</Link>
                  </li>
                  <li>
                    <Link to="/privacy-policy">Privacy Policy</Link>
                  </li>
                  <li>
                    <a href="https://m.me/loopinglobal?ref=src--WEB" target="_blank">Contact Us</a>
                  </li>
                  <li className="socail-icon">
                    <a
                      href="https://www.facebook.com/loopinglobal/"
                      className="fb social-icon"
                      target="_blank"
                    >
                      <img src={Facebook} alt="" />
                      <img src={FacebookHover} alt="" className="hovered" />
                    </a>
                    <a
                      href="https://www.instagram.com/loopinglobal"
                      className="insta social-icon"
                      target="_blank"
                    >
                      <img src={Insta} alt="" />
                      <img src={InstaHover} alt="" className="hovered" />
                    </a>

                    <a
                      href="https://www.linkedin.com/company/loopinglobal/about/"
                      className="in social-icon"
                      target="_blank"
                    >
                      <img src={Linkedin} alt="" />
                      <img src={LinkedinHover} alt="" className="hovered" />
                    </a>
                    <a
                      href="https://twitter.com/Loopinglobal"
                      className="twitter social-icon"
                      target="_blank"
                    >
                      <img src={Twitter} alt="" />
                      <img src={TwitterHover} alt="" className="hovered" />
                    </a>
                    <a
                      href="https://www.youtube.com/channel/UC4EYSxR2nheO637umzUnxnQ"
                      className="youtube social-icon"
                      target="_blank"
                    >
                      <img src={Youtube} alt="" />
                      <img src={YoutubeHover} alt="" className="hovered" />
                    </a>
                  </li>
                </ul>
              </div>
            </Col>
            <Col sm={4}>
              <div className="footer-logo">
                <img src={Logo} alt="logo" />
              </div>
            </Col>
          </Row>
        </Container>
        <a className="chatbox" href="https://m.me/loopinglobal?ref=src--WEB" target="_blank">
          <img src={FbMesseneger} />
          <span>Talk to us</span>
        </a>
      </div>
    );
  }
}
export default withRouter(footer);
