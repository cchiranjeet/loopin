import React from "react";
// import Video from "../../assets/img/SampleVideo.mp4";
// import PosterImage1 from "../../assets/img/poster1.png";

class Popup extends React.Component {
  render() {
    return (
      <div className="popup">
        <div className="popup_inner bg-black">
          {this.props.videoType === 'youtube' ?
            (
              <iframe width="100%" height="100%" src={`https://www.youtube.com/embed/${this.props.videoId}`} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            )
            :
            (
              <video
                // poster={PosterImage1}
                src={this.props.url}
                id="video-bg"
                // autoPlay
                // loop
                // muted
                // webkit-playsinline="true"
                // playsinline="true"
                controls
                className="bg-black"
              />
            )
          }
          <span className="vodal-close" onClick={this.props.closePopup} />
        </div>
      </div>
    );
  }
}

export default Popup;
