/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import logo from "../../assets/img/blackLogo.svg";
import { Link, withRouter } from "react-router-dom";
// import { slide as Menu } from "react-burger-menu";
import { ReactComponent as Cart } from "../../assets/img/cart.svg";
import { Container, Row, Col, Hidden, Visible } from "react-grid-system";
import { slide as Menu } from "react-burger-menu";
import * as firebase from "firebase";
import "firebase/auth";

const iosScrollListener = callBack => {
  const element = document.createElement("div");
  element.id = "hidden";
  element.style = "position: absolute; opacity: 0; top:-30px";
  document.body.append(element);

  const fn = () => {
    element.innerHTML = window.scrollY;
    callBack();
  };
  window.addEventListener("scroll", fn);

  return () => window.removeEventListener("scroll", fn);
};

class Header extends Component {
  constructor() {
    super();
    this.state = {
      showMenu: false,
      menuOpen: false,
      clicked: [],
      isTop: true
    };
  }
  handleStateChange(state) {
    this.setState({ menuOpen: state.isOpen });
  }
  componentDidMount() {
    this.remove = iosScrollListener(() => {
      const offset = 50;
      if (!this.headerRef) {
        return;
      }
      if (window.scrollY > offset) {
        this.headerRef.classList.add("active");
      } else {
        this.headerRef.classList.remove("active");
      }
    });
  }

  signout() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.props.setLoggedIn(null);
      });
  }
  componentWillMount() {
    document.addEventListener("click", this.handleOutsideClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleOutsideClick, false);
    this.remove();
  }

  handleClick = (index, e) => {
    let clicked = this.state.clicked;
    clicked[index] = !clicked[index];
    this.setState({ clicked: clicked });
  };

  handleOutsideClick = event => {
    // if (!this.refs.megaMenu.contains(event.target)) {
    //   this.setState({
    //     clicked: []
    //   });
    // }
  };

  render() {
    const styles = {
      bmBurgerButton: {
        position: "fixed",
        width: "26px",
        height: "18px",
        left: "15px",
        top: "16px"
      },
      bmBurgerBars: {
        background: "#4a4e4d",
        height: "16%"
      },
      bmBurgerBarsHover: {
        background: "#a90000"
      },
      bmCrossButton: {
        height: "40px",
        width: "40px"
      },
      bmCross: {
        background: "#4a4e4d",
        height: "22px"
      },
      bmMenuWrap: {
        position: "fixed",
        height: "100%"
      },
      bmMenu: {
        background: "#fff",
        fontSize: "1.15em"
      },
      bmMorphShape: {
        fill: "#373a47"
      },
      bmItemList: {
        color: "#4a4e4d",
        padding: "60px 0 20px 0"
      },
      bmItem: {
        display: "inline-block",
        outline: "none",
        width: "100%",
        opacity: "0.7",
        color: "#000",
        fontWeight: "500",
        padding: "6px 50px"
      },
      bmOverlay: {
        background: "rgba(0, 0, 0, 0.8)"
      }
    };
    const { location } = this.props;

    const aboutClass = location.pathname.match(/^\/about-us/) ? "active" : "";
    const productClass = location.pathname.match(/^\/whyloopin|ebook|course|superbot/)
      ? "active"
      : "";
    const loopin = location.pathname.match(/^\/whyloopin/) ? "active" : "";
    const ebook = location.pathname.match(/^\/.*-ebook$/) ? "active" : "";
    const course = location.pathname.match(/^\/.*-course/) ? "active" : "";
    const superbot = location.pathname.match(/^\/superbot/) ? "active" : "";
    const price = location.pathname.match(/^\/price-plan/) ? "active" : "";
    const blog = location.pathname.match(/^\/blog/) ? "active" : "";
    return (
      <div>
        <Hidden sm xs>
          <div
            className="header-component"
            ref={r => {
              this.headerRef = r;
            }}
          >
            <Container fluid style={{ maxWidth: "1200px" }}>
              <Row>
                <Col sm={2}>
                  <Link to="/" className="logo">
                    <img src={logo} alt="logo" />
                  </Link>
                </Col>
                <Col sm={10}>
                  <div className="nav">
                    <ul>
                      <li className={aboutClass}>
                        <Link to="/about-us" activeClassName="active">
                          About
                        </Link>
                      </li>
                      <li className={"dropdown " + productClass}>
                        <div className="dropbtn">
                          Product <span className="arrow-down" />
                        </div>
                        <div class="dropdown-content ">
                          <Link
                            to="/whyloopin"
                            className={loopin}
                            activeClassName="active"
                          >
                            Loopin
                          </Link>
                          <Link
                            to="/network-marketing-5.0-ebook"
                            className={ebook}
                            activeClassName="active"
                          >
                            Handbook
                          </Link>
                          <Link
                            to="/network-marketing-course"
                            className={`p-half--sides ${course}`}
                            activeClassName="active"
                          >
                            Success Blueprint Course
                          </Link>
                          {/* <Link
                            to="/superbot"
                            className={superbot}
                            activeClassName="active"
                          >
                            Superbot
                          </Link> */}
                        </div>
                      </li>
                      <li className={price}>
                        <Link to="/price-plan" activeClassName="active">
                          Pricing
                        </Link>
                      </li>
                      <li className={blog}>
                        <Link to="/blog">Blog</Link>
                      </li>
                      <li>
                        <a href="https://academy.loopinglobal.com/" target="_blank">Looopin Academy</a>
                      </li>
                      <li>
                        {!this.props.loggedIn ? (
                          <a href="https://academy.loopinglobal.com/users/sign_in" target="_blank">
                            <div className="LoginBtn">Login</div>
                          </a>
                        ) : (
                            <>
                              <div className="dropbtn">
                                {this.props.loggedIn.displayName ||
                                  this.props.loggedIn.email}
                                <span className="arrow-down" />
                              </div>
                              <div class="dropdown-content">
                                <div
                                  onClick={() => {
                                    window.location.href =
                                      "https://loopin.thinkific.com/";
                                  }}
                                >
                                  My Courses
                              </div>
                                <div onClick={() => this.signout()}>Signout</div>
                              </div>
                            </>
                          )}
                      </li>
                      {/* <li className="cart">
                        <Link to="/cart">
                          <Cart />
                          <div className="count">
                            <span>{this.props.cartValue.length}</span>
                          </div>
                        </Link>
                      </li>
                      <Hidden xs sm>
                        <li className="language" ref="megaMenu">
                          <div onClick={() => this.handleClick(1)}>
                            <span className="circle" /> ENG
                            <span className="arrow-down" />
                          </div>
                          <div
                            className={
                              "mega-menu" + " " + this.state.clicked[1]
                            }
                          >
                            <div className="mega-menu-content">
                              <p>Hindi</p>
                            </div>
                          </div>
                        </li>
                      </Hidden> */}
                    </ul>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
        </Hidden>

        <Visible sm xs>
          <div
            className="header-mobile"
            ref={r => {
              this.headerRef = r;
            }}
          >
            <Row>
              <Col xs={3}>
                <Menu
                  styles={styles}
                  isOpen={this.state.menuOpen}
                  onStateChange={state => this.handleStateChange(state)}
                >
                  <Link
                    className={"menu-item " + aboutClass}
                    to="/about-us"
                    onClick={() => this.setState({ menuOpen: false })}
                  >
                    About
                  </Link>
                  <Link
                    to="/whyloopin"
                    className={"menu-item " + loopin}
                    onClick={() => this.setState({ menuOpen: false })}
                  >
                    Loopin
                  </Link>
                  <Link
                    className={"menu-item " + ebook}
                    to="/network-marketing-5.0-ebook"
                    onClick={() => this.setState({ menuOpen: false })}
                  >
                    Handbook
                  </Link>
                  <Link
                    className={"menu-item " + course}
                    to="/network-marketing-course"
                    onClick={() => this.setState({ menuOpen: false })}
                  >
                    Success Blueprint Course
                  </Link>
                  {/* <Link
                    className={"menu-item " + superbot}
                    to="/Superbot"
                    onClick={() => this.setState({ menuOpen: false })}
                  >
                    Superbot
                  </Link> */}
                  <Link
                    className={"menu-item " + price}
                    to="/price-plan"
                    onClick={() => this.setState({ menuOpen: false })}
                  >
                    Pricing
                  </Link>
                  <Link
                    className={"menu-item " + blog}
                    to="/blog"
                    onClick={() => this.setState({ menuOpen: false })}
                  >
                    Blog
                  </Link>
                  <a href="https://academy.loopinglobal.com/" target="_blank">Looopin Academy</a>
                  {!this.props.loggedIn ? (
                    <a href="https://academy.loopinglobal.com/users/sign_in" target="_blank">
                      <div className="">Login</div>
                    </a>
                    // <Link
                    //   className="menu-item"
                    //   to="/signup"
                    //   ref="megaMenu"
                    //   onClick={() => this.setState({ menuOpen: false })}
                    // >
                    //   Login/Signup
                    // </Link>
                  ) : (
                      <div
                        ref="megaMenu"
                        onClick={() => this.signout()}
                        className="signout"
                      >
                        Signout
                      </div>
                    )}
                </Menu>
              </Col>
              <Col xs={6}>
                <Link to="/" className="logo">
                  <img src={logo} alt="logo" />
                </Link>
              </Col>
              {/* <Col xs={3}>
                <div className="cart">
                  <Link to="/cart">
                    <Cart />
                  </Link>
                </div>
              </Col> */}
            </Row>
          </div>
        </Visible>
      </div>
    );
  }
}
export default withRouter(Header);
