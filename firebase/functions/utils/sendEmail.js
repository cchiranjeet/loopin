
const nodemailer = require('nodemailer');

async function sendEmail(params) {
  /**
  * Here we're using Gmail to send 
  */
  let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'support@loopinglobal.com',
      pass: 'ppvlnzbtpustgvbd'
    }
  });

  const mailOptions = {
    from: 'Loopin Global', // Something like: Jane Doe <janedoe@gmail.com>
    to: params.to,
    subject: params.subject, // email subject
    html: params.html
  };

  // returning result
  return new Promise(resolve => {
    transporter.sendMail(mailOptions, (erro, info) => {
      console.log(erro)
      return resolve(erro ? erro.toString() : true);
    });
  });
}

module.exports = sendEmail;