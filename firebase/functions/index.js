const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const path = require('path');
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
const serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://loopin-fad04.firebaseio.com"
});

const app = express();
const views = path.join(__dirname, "./views");
const sendEmail = require("./utils/sendEmail");

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
app.set("view engine", "pug");
app.set("views", views);

// Add middleware to authenticate requests
// app.use(myMiddleware);

app.get('/getUserByEmail', async (req, res, next) => {
  try {
    const email = req.query.email;
    const user = await admin.auth().getUserByEmail(email);
    return res.send(user);
  } catch (err) {
    return res.send(false);
  }
});

app.post('/ebookPayment', async (req, res, next) => {
  try {
    const emailParams = { to: req.body.to, subject: 'Welcome on board' };
    emailParams.html = await new Promise(resolve => {
      res.render(`emails/ebookPayment`, req.body, (err, html) => {
        resolve(err ? null : html);
      });
    });
    const response = await sendEmail(emailParams);
    return res.send(response);
  } catch (err) {
    return next(err);
  }
});

app.post('/trainingPayment', async (req, res, next) => {
  try {
    const emailParams = { to: req.body.to, subject: 'Transaction confirmed' };
    emailParams.html = await new Promise(resolve => {
      res.render(`emails/trainingPayment`, req.body, (err, html) => {
        return resolve(err ? null : html);
      });
    });
    const response = await sendEmail(emailParams);
    return res.send(response);
  } catch (err) {
    return next(err);
  }
});

app.post('/sendEmail', async (req, res, next) => {
  try {
    const emailParams = {
      to: req.body.to,
      subject: req.body.subject,
      html: req.body.html
    };
    const response = await sendEmail(emailParams);
    return res.send(response);
  } catch (err) {
    return next(err);
  }
});

app.get('/bot', async (req, res, next) => {
  try {
    const plan = req.query.plan;
    const plans = {
      basic: {
        name: 'Loopin Basic',
        price: 19997
      }
    }
    return res.send(plans[plan]);
  } catch (err) {
    return res.send(false);
  }
});

app.get('/bot-subscription', async (req, res, next) => {
  try {
    const email = req.query.email;
    const botSubscription = await new Promise(resolve => {
      return admin.firestore().collection(`subscriptions`).where('email_id', '==', email).limit(1).get()
        .then(snapshot => {
          let result = null;
          snapshot.forEach(doc => {
            result = doc.data();
          });
          return resolve(result);
        });
    });
    return res.json(botSubscription);
  } catch (err) {
    console.log(err)
    return res.send(false);
  }
});

app.post('/purchase-bot-subscription', async (req, res, next) => {
  try {
    console.log(req.body);
    const plan = req.body.plan;
    const db = admin.firestore();
    const uniqueLink = `https://m.me/loopinglobal?ref=rpay--${req.body.razorpay_payment_id}`;
    const payment = await db.collection(`payments`).add({
      email_id: req.body.email,
      razorpay_payment_id: req.body.razorpay_payment_id,
      amount: req.body.amount,
      currency: "INR",
      date: new Date()
    });
    const botSubscription = await db.collection('subscriptions').add({
      email_id: req.body.email,
      username: req.body.username,
      mobileno: req.body.mobileno,
      link: `https://m.me/loopinglobal?ref=rpay--${req.body.razorpay_payment_id}`,
      payment_id: payment.id,
      plan: plan,
      date: new Date()
    });
    const emailParams = { to: req.body.email, subject: 'Subscription confirmed' };
    emailParams.html = await new Promise(resolve => {
      res.render(`emails/botSubscription`, { ...req.body, uniqueLink }, (err, html) => {
        return resolve(err ? null : html);
      });
    });
    const response = await sendEmail(emailParams);
    return res.send(response);
  } catch (err) {
    return res.send(false);
  }
});

// Expose Express API as a single Cloud Function:
exports.functions = functions.https.onRequest(app);
